Keyboard controls:

i			camera zoom in
o			camera zoom out
q/Q/ESC		quit

--------------------------------------------------------------------------------------------------------------

Notes:  Unless otherwise noted, this program implements all requirements of the assignment and extra credit.

--------------------------------------------------------------------------------------------------------------

Environment Tested on: Windows Vista x64, nvidia GeForce 9800, OpenGL version 3.3

Using the property sheets included with the project, everything should build as long as OpenGL
is installed.  The static libraries, dlls, and headers for glut, glew, and DevIL are included in the project
in the lib, dll, and include folders, respectively.  If it doesn't build, follow the instructions below.


Installing GLEW and GLUT

1. Download a Visual Studio 2010 compatible version of the GLEW source from https://github.com/chrisoei/glew
2. Download the latest version of GLUT binaries from http://www.opengl.org/resources/libraries/glut/glut_downloads.php
3. Build GLEW in visual studio 2010.  It will produce the files glew32d.dll and glew32d.lib.
4. Locate glut32.lib and glut32.dll binaries.
5. Move the .lib files to {VisualStudioRoot}\lib
6. Move the .dll files to the location where the executable requiring them will be run, or alternatively to C:\windows\system
7. Find glut.h, glew.h, and wglew.h.
8. Move the header files to {visual studio root}\include\GL

building the executable with visual studio 2010

1. Create an empty project.
2. In "project properties" -> "linker" -> "input" -> "additional dependencies" add glut32.lib and glew32d.lib
3. Make sure fshader21.glsl and vshader21.glsl are in the same directory as the executable, so that a call to
	fopen('fshader21.glsl', 'r') will be sufficient to find the file's location.  If the executable is run
	from visual studio (for example you hit ctrl + F5 for build and run) then the shader files may need to be
	in the same directory as the .vcxproj file.
4.  Angel.h must be in the same directory as main.cpp to link properly.
5.  I have included the executable, glut32.dll, glew32d.dll, and shaders in a folder called "Standalone", in
	case there is trouble building the project.
6.  I have also included glut32.lib and glew32d.lib in a folder called lib, in case these are needed for
	some reason.