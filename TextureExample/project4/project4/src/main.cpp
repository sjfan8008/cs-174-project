// rotating cube with two texture objects
// change textures with 1 and 2 keys

#include "Angel.h"
#include "IL\ilut.h"

const int  NumTriangles = 4; // (1 face per rectangle) * (2 triangles per face) * 2 rectangles
const int  NumVertices  = 3 * NumTriangles;
const int  TextureSize  = 64;

typedef Angel::vec4 point4;
typedef Angel::vec4 color4;

// Texture objects and storage for texture image
enum {NOT_ZOOMED=0, ZOOMED=1};
GLuint textures[2];

GLubyte* image;

// Vertex data arrays
point4  points[NumVertices];
color4  quad_colors[NumVertices];
vec2    tex_coords[NumVertices];

GLuint vao1, vao2;

GLuint   modelView;
point4 eye;

//----------------------------------------------------------------------------

int Index = 0;

void
quad( int a, int b, int c, int d, GLfloat zoom )
{
	if (abs(zoom) < DivideByZeroTolerance)
	{
	    std::cerr << "[" << __FILE__ << ":" << __LINE__ << "] "
		      << "Division by zero" << std::endl;
		exit(1);
	}

    point4 vertices[4] = {
		point4(-.5, .5, 0, 1.0),
		point4(-.5, -.5, 0, 1.0),
		point4(.5, -.5, 0, 1.0),
		point4(.5, .5, 0, 1.0)
    };

	//a -> 0,0
	//b -> 0,1
	//c -> 1,1
	//d -> 1,0

    color4 colors[4] = {
	color4( 0.0, 0.0, 0.0, 1.0 ),  // black
	color4( 1.0, 0.0, 0.0, 1.0 ),  // red
	color4( 1.0, 1.0, 0.0, 1.0 ),  // yellow
	color4( 0.0, 1.0, 0.0, 1.0 )   // green
    };

	//zoom of .5 means the width and height of the area that
	//one instance of the texture is mapped to is 50% the width of 
	//the rectangle.  Thus the full range of (s,t) in texture coordinates
	//that will be mapped to the rectangle is 1/zoom.  The difference in 
	//width or height will be newDimension - originalDimension = 1/zoom - 1.0,
	//and on each side of the area the texture is being mapped to, we must
	//expand or shrink the (s,t) boundaries by half this amount

	GLfloat newWidth = 1.0 / zoom;
	GLfloat newHeight = 1.0 / zoom;
	GLfloat widthDiff = newWidth - 1.0;
	GLfloat heightDiff = newHeight - 1.0;

	//expand sampling area by one half widthDiff on both sides
	GLfloat sMin = 0 - 0.5 * widthDiff;
	GLfloat sMax = 1.0 + 0.5 * widthDiff;
	GLfloat tMin = 0 - 0.5 * heightDiff;
	GLfloat tMax = 1.0 + 0.5 * heightDiff;

    quad_colors[Index] = colors[a];
    points[Index] = vertices[a];
    tex_coords[Index] = vec2( sMin, tMin );
    Index++;
    
    quad_colors[Index] = colors[a];
    points[Index] = vertices[b];
    tex_coords[Index] = vec2( sMin, tMax );
    Index++;
    
    quad_colors[Index] = colors[a];
    points[Index] = vertices[c];
    tex_coords[Index] = vec2( sMax, tMax );
    Index++;
    
    quad_colors[Index] = colors[a];
    points[Index] = vertices[a];
    tex_coords[Index] = vec2( sMin, tMin );
    Index++;
    
    quad_colors[Index] = colors[a];
    points[Index] = vertices[c];
    tex_coords[Index] = vec2( sMax, tMax );
    Index++;
    
    quad_colors[Index] = colors[a];
    points[Index] = vertices[d];
    tex_coords[Index] = vec2( sMax, tMin );
    Index++;
}

//----------------------------------------------------------------------------

void
rectangle(GLfloat zoom)
{
    quad( 0, 1, 2, 3, zoom );
}

//----------------------------------------------------------------------------

void
init()
{
	//load image
	ILuint ImageName;
	ilGenImages(1, &ImageName);
	ilBindImage(ImageName);

	ilLoadImage("brick.jpg");

	ILuint Width, Height;
	Width = ilGetInteger(IL_IMAGE_WIDTH);
	Height = ilGetInteger(IL_IMAGE_HEIGHT);
	image = new GLubyte[Width*Height*3];

	ilCopyPixels(0, 0, 0, Width, Height, 1, IL_RGB, IL_UNSIGNED_BYTE, image);

	ilBindImage(0);
	ilDeleteImage(ImageName);

	//generate rectangle geometry
	rectangle(1.0);
    rectangle(0.5);

    // Initialize texture objects
    glGenTextures( 2, textures );

    glBindTexture( GL_TEXTURE_2D, textures[NOT_ZOOMED] );
    glTexImage2D( GL_TEXTURE_2D, 0, GL_RGB, Width, Height, 0,
		  GL_RGB, GL_UNSIGNED_BYTE, image );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );

    glBindTexture( GL_TEXTURE_2D, textures[ZOOMED] );
    glTexImage2D( GL_TEXTURE_2D, 0, GL_RGB, Width, Height, 0,
		  GL_RGB, GL_UNSIGNED_BYTE, image );
	glGenerateMipmap(GL_TEXTURE_2D);
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR );

    glActiveTexture( GL_TEXTURE0 );
    glBindTexture( GL_TEXTURE_2D, textures[NOT_ZOOMED] );

    // Create and initialize a buffer object
    GLuint buffer;
    glGenBuffers( 1, &buffer );
    glBindBuffer( GL_ARRAY_BUFFER, buffer );
    glBufferData( GL_ARRAY_BUFFER,
		  sizeof(points) + sizeof(quad_colors) + sizeof(tex_coords),
		  NULL, GL_STATIC_DRAW );

    // Specify an offset to keep track of where we're placing data in our
    //   vertex array buffer.  We'll use the same technique when we
    //   associate the offsets with vertex attribute pointers.
    GLintptr offset = 0;
    glBufferSubData( GL_ARRAY_BUFFER, offset, sizeof(points), points );
    offset += sizeof(points);

    glBufferSubData( GL_ARRAY_BUFFER, offset,
		     sizeof(quad_colors), quad_colors );
    offset += sizeof(quad_colors);
    
    glBufferSubData( GL_ARRAY_BUFFER, offset, sizeof(tex_coords), tex_coords );

    // Load shaders and use the resulting shader program
    GLuint program = InitShader( "vshader.glsl", "fshader.glsl" );
    glUseProgram( program );

    // set up vertex arrays for rectangle 1
    glGenVertexArrays( 1, &vao1 );
    glBindVertexArray( vao1 );

    offset = 0;
    GLuint vPosition = glGetAttribLocation( program, "vPosition" );
    glEnableVertexAttribArray( vPosition );
    glVertexAttribPointer( vPosition, 4, GL_FLOAT, GL_FALSE, 0,
			   BUFFER_OFFSET(offset) );
    offset += sizeof(points);

    GLuint vColor = glGetAttribLocation( program, "vColor" ); 
    glEnableVertexAttribArray( vColor );
    glVertexAttribPointer( vColor, 4, GL_FLOAT, GL_FALSE, 0,
			   BUFFER_OFFSET(offset) );
    offset += sizeof(quad_colors);

    GLuint vTexCoord = glGetAttribLocation( program, "vTexCoord" );
    glEnableVertexAttribArray( vTexCoord );
    glVertexAttribPointer( vTexCoord, 2, GL_FLOAT, GL_FALSE, 0,
			   BUFFER_OFFSET(offset) );

	glBindVertexArray(0);
	glGenVertexArrays(1, &vao2);
	glBindVertexArray(vao2);

	// set up vertext arrays for rectangle 2
	offset = sizeof(points) / 2;
    glEnableVertexAttribArray( vPosition );
    glVertexAttribPointer( vPosition, 4, GL_FLOAT, GL_FALSE, 0,
			   BUFFER_OFFSET(offset) );
    offset = sizeof(points) + sizeof(quad_colors) / 2;

    glEnableVertexAttribArray( vColor );
    glVertexAttribPointer( vColor, 4, GL_FLOAT, GL_FALSE, 0,
			   BUFFER_OFFSET(offset) );
    offset = sizeof(points) + sizeof(quad_colors) + sizeof(tex_coords)/2;

    glEnableVertexAttribArray( vTexCoord );
    glVertexAttribPointer( vTexCoord, 2, GL_FLOAT, GL_FALSE, 0,
			   BUFFER_OFFSET(offset) );

	glBindVertexArray(0);

    // Set the value of the fragment shader texture sampler variable
    //   ("texture") to the the appropriate texture unit. In this case,
    //   zero, for GL_TEXTURE0 which was previously set by calling
    //   glActiveTexture().
    glUniform1i( glGetUniformLocation(program, "texture"), 0 );

    modelView = glGetUniformLocation( program, "modelView" );

	//camera position
	eye = point4(0,0,1.0,1.0);

	//perspective projection
	glUniformMatrix4fv(glGetUniformLocation(program, "projection"), 1, GL_TRUE, Perspective(60, 1.0, .1, 60));
    
    glEnable( GL_DEPTH_TEST );
    
    glClearColor( 1.0, 1.0, 1.0, 1.0 );
}

void
display( void )
{
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	 
	point4 at = eye + point4(0,0,-1,1);
	vec4 up(0,1,0,0);

	mat4 cMw = LookAt(eye, at, up);

	//draw zoomed in rectangle
	glBindTexture( GL_TEXTURE_2D, textures[NOT_ZOOMED] );
	mat4 cMm = cMw * Translate(-.5,0,0) * Scale(.75);
	glUniformMatrix4fv(modelView, 1, GL_TRUE, cMm);
	glBindVertexArray(vao1);
    glDrawArrays( GL_TRIANGLES, 0, NumVertices/2 );
	glBindVertexArray(0);

	//draw zoomed out rectangle
	glBindTexture( GL_TEXTURE_2D, textures[ZOOMED] );
	cMm = cMw * Translate(.5,0,0) * Scale(.75);
	glUniformMatrix4fv(modelView, 1, GL_TRUE, cMm);
	glBindVertexArray(vao2);
	glDrawArrays( GL_TRIANGLES, 0, NumVertices/2 );
	glBindVertexArray(0);

    glutSwapBuffers();
}

//----------------------------------------------------------------------------

void
idle( void )
{   
    glutPostRedisplay();
}

//----------------------------------------------------------------------------

void
keyboard( unsigned char key, int mousex, int mousey )
{
    switch( key ) {
	case 033: // Escape Key
	case 'q': case 'Q':
	    exit( EXIT_SUCCESS );
	    break;
	case 'i':
	    eye.z -= .1;
	    break;

	case 'o':
	    eye.z += .1;
	    break;
    }

    glutPostRedisplay();
}

//----------------------------------------------------------------------------

int
main( int argc, char **argv )
{
    glutInit( &argc, argv );
    glutInitDisplayMode( GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH );
    glutInitWindowSize( 512, 512 );
    glutCreateWindow( "Color Cube" );

    glewInit();
	ilInit();

    init();

    glutDisplayFunc( display );
    glutKeyboardFunc( keyboard );
    glutIdleFunc( idle );

    glutMainLoop();
    return 0;
}
