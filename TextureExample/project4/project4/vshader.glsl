#version 150

in  vec4 vPosition;
in  vec4 vColor;
in  vec2 vTexCoord;

out vec4 color;
out vec2 texCoord;

uniform mat4 modelView;
uniform mat4 projection;

void main() 
{   
    color       = vColor;
    texCoord    = vTexCoord;
    gl_Position = projection * modelView * vPosition;
} 