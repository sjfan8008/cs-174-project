#version 120

// per-fragment interpolated values from the vertex shader
varying vec3 fN;
varying vec3 fE;
varying vec3 fL;

in vec2 texCoord;
in vec4 color;

uniform sampler2D texture;

uniform vec4 AmbientProduct, DiffuseProduct, SpecularProduct;
uniform mat4 ModelView;
uniform vec4 LightPosition;
uniform float Shininess;

uniform int ShadingType;
uniform int renderMode;

uniform vec4 pickColor;

void main( void )
{
	if (renderMode == 2) {
		//color buffer picking
		gl_FragColor = pickColor;
		return;
	}

	if (ShadingType == 2) {
		//Phong shading: interpolate normals

		// normalize the input lighting vectors
		vec3 N = normalize( fN );
		vec3 E = normalize( fE );
		vec3 L = normalize( fL );

	    vec3 H = normalize( L + E );

	    vec4 ambient = AmbientProduct;

	    float Kd = max( dot( L, N ), 0.0 );
		vec4 diffuse = Kd * DiffuseProduct;

	    float Ks = pow( max( dot( N, H ), 0.0 ), Shininess );
	    vec4 specular = Ks * SpecularProduct;

	    // discard the specular highlight if the light is behind the vertex
		if ( dot( L, N ) < 0.0 )
		{
			specular = vec4( 0.0, 0.0, 0.0, 1.0 );
		}

	    gl_FragColor = (ambient + diffuse + specular) * texture2D( texture, texCoord );
	    gl_FragColor.a = .4;
	}
	else {
		//flat / gouraud shading:

		//interpolate colors, not normals.  See comments in vertex shader
		//for why it is ok to interpolate colors when doing flat shading.
		gl_FragColor = color * texture2D( texture, texCoord );
	}
}
