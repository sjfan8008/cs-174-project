#version 120

attribute vec4 vPosition;
attribute vec3 vNormal;
attribute vec2 vTexCoord;

// output values will be interpreted per-fragment for Phong shading
varying vec3 fN;
varying vec3 fE;
varying vec3 fL;

//texture coordinate
varying vec2 texCoord;

//0 indicated flat shading, 1 gourand shading, 2 phong shading
uniform int ShadingType;

//1 for normal rendering, 2 for color buffer picking
uniform int renderMode;

//transformations
uniform mat4 ModelView;
uniform mat4 Camera;
uniform mat4 Projection;

//lighting
uniform vec4 AmbientProduct, DiffuseProduct, SpecularProduct;
uniform vec4 LightPosition;
uniform float Shininess;
varying vec4 color;

void main( void )
{
	if (renderMode == 2) {
		//color buffer picking.  Render only 1 color
		gl_Position = Projection * ModelView * vPosition;
		return;
	}

	texCoord = vTexCoord;

	if (ShadingType == 2) {

	//Phong shading: calculate normals to be interpolated by
	//the fragment shader.

		fN = (ModelView*vec4(vNormal,0)).xyz;
		fE = (-ModelView*vPosition).xyz;
		fL = (Camera*LightPosition).xyz;

		if ( LightPosition.w != 0.0 )
		{
			fL = (Camera*LightPosition).xyz - (ModelView*vPosition).xyz;
		}

		gl_Position = Projection * ModelView * vPosition;
	}
	else if (ShadingType == 1 || ShadingType == 0) {

	//Calculate a color for this vertex and send it to the 
	//fragment shader to be interpolated.  We can use this
	//method for both flat and gouraud shading.  Even though
	//technically for flat shading we should use a point in 
	//the center of the polygon as a measure of the light's distance
	//from the surface, since the polygons are close together
	//it is ok to calculate colors per vertex, as they will all
	//come out about the same.

		// Transform vertex  position into eye coordinates
		vec3 pos = (ModelView * vPosition).xyz;
	
		vec3 L = normalize( (Camera*LightPosition).xyz - pos );
		vec3 E = normalize( -pos );
		vec3 H = normalize( L + E );

		// Transform vertex normal into eye coordinates
		vec3 N = normalize( ModelView*vec4(vNormal, 0.0) ).xyz;

		// Compute terms in the illumination equation
		vec4 ambient = AmbientProduct;

	    float Kd = max( dot(L, N), 0.0 );
		vec4  diffuse = Kd*DiffuseProduct;

	    float Ks = pow( max(dot(N, H), 0.0), Shininess );
		vec4  specular = Ks * SpecularProduct;
    
	    if( dot(L, N) < 0.0 ) {
		specular = vec4(0.0, 0.0, 0.0, 1.0);
		} 

	    gl_Position = Projection * ModelView * vPosition;

		color = ambient + diffuse + specular;
		color.a = 1.0;
	}
}
