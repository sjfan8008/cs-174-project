#ifndef INPUT_H
#define INPUT_H

#include "Angel.h"

//These codes replace the GLUT special keys codes.
//The idea is that we don't want to have a special function
//to handle these keys just because their values overlap
//with some ASCII codes.
const GLuint KEY_F1 = 256 + GLUT_KEY_F1;
const GLuint KEY_F2 = 256 + GLUT_KEY_F2;
const GLuint KEY_F3 = 256 + GLUT_KEY_F3;
const GLuint KEY_F4 = 256 + GLUT_KEY_F4;
const GLuint KEY_F5 = 256 + GLUT_KEY_F5;
const GLuint KEY_F6 = 256 + GLUT_KEY_F6;
const GLuint KEY_F7 = 256 + GLUT_KEY_F7;
const GLuint KEY_F8 = 256 + GLUT_KEY_F8;
const GLuint KEY_F9 = 256 + GLUT_KEY_F9;
const GLuint KEY_F10 = 256 + GLUT_KEY_F10;
const GLuint KEY_F11 = 256 + GLUT_KEY_F11;
const GLuint KEY_F12 = 256 + GLUT_KEY_F12;
const GLuint KEY_LEFT = 256 + GLUT_KEY_LEFT;
const GLuint KEY_UP = 256 + GLUT_KEY_UP;
const GLuint KEY_RIGHT = 256 + GLUT_KEY_RIGHT;
const GLuint KEY_DOWN = 256 + GLUT_KEY_DOWN;
const GLuint KEY_PAGE_UP = 256 + GLUT_KEY_PAGE_UP;
const GLuint KEY_PAGE_DOWN = 256 + GLUT_KEY_PAGE_DOWN;
const GLuint KEY_HOME = 256 + GLUT_KEY_HOME;
const GLuint KEY_END = 256 + GLUT_KEY_END;
const GLuint KEY_INSERT = 256 + GLUT_KEY_INSERT;

class KeyboardState
{
public:
	KeyboardState();

	virtual ~KeyboardState();

	KeyboardState(const KeyboardState& other);

	KeyboardState& operator=(const KeyboardState& other);

	/*  Record the state of a particular key.
	*	@param key: the code of the key to record.
	*	@param down: true if the key is down, false otherwise.
	*/
	void setKey(GLuint key, bool down);

	/*  Record the state of a special key.
	*	@param key: the code of the key to record.  Must be one of
	*		GLUT special keys.
	*	@param down: true if the key is down, false otherwise.
	*/
	void setSpecialKey(GLuint key, bool down);

	/*  Returns true if a particular key is down.
	*	@param key: the code of the key.  This should be given
	*		as its character representation, for exampled 'a', or
	*		if the key is a GLUT special key, as a constant preceded
	*		by KEY_, for example, KEY_UP if the key to query is GLUT_KEY_UP.
	*/
	bool isDown(GLuint key) const;

private:
	const static int SIZE = 512;
	bool *m_keys;
	bool *m_specialKeys;
};

#endif