#ifndef LIGHTING_H
#define LIGHTING_H

#include "Angel.h"
typedef vec4 color4;

class LightSource
{
public:
	LightSource(const vec3& position, const color4& specular, const color4& diffuse, const color4& ambient)
		: m_position(position), m_specular(specular), m_diffuse(diffuse), m_ambient(ambient)
	{}

	color4 ambientProductOn(const color4& other_color) const { return m_ambient * other_color; }
	color4 specularProductOn(const color4& other_color) const { return m_specular * other_color; }
	color4 diffuseProductOn(const color4& other_color) const { return m_diffuse * other_color; }

	vec3 getPosition() const { return m_position; }
	void setPosition(const vec3& p) { m_position = p; }

private:
	vec3 m_position;
	color4 m_specular;
	color4 m_diffuse;
	color4 m_ambient;
};

#endif