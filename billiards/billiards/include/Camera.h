#ifndef CAMERA_H
#define CAMERA_H

#include "Angel.h"
#include "Quaternion.h"

class Camera3D
{
public:

	/* Constructor:
	*  @param center: the postition of the camera
	*  @param roll: roll of the camera.  0 roll defined an 'up' vector of [0,1,0]
	*  @param yaw: Camera yaw.  0 yaw is defined as aligned with the z-y plane, facing -z
	*  @param pitch: Camera pitch. 0 pitch is defined as aligned with the x-z plane.
	*  @param nearClip: distance of the nearclipping plane from the camera
	*  @param farClipL: distance of the farclipping plane from the camera
	*  @param fovy: vertical field of view.
	*  @param aspect: aspect ration
	*/
	Camera3D(const vec3& center, GLfloat roll, GLfloat yaw, GLfloat pitch, 
			 GLfloat nearClip, GLfloat farClip, GLfloat fovy, GLfloat aspect);

	/* Tells this camera what shader variable to send it's matrix to.  This function 
	*	must be called before useView().
	*
	*  @param pMwMatrix: This must be the location of a shader uniform attribute location of a mat4.
	*		The shader variable itself should represent a transformation from world coordinates to
	*		projected coordinates.
	*/
	void useShaderMatrix(GLuint pMwMatrix);

	/*  Returns the current position of the camera.
	*/
	vec3 getPosition() const;

	/*  Change the near-clipping, far-clipping, vertical field of view, and aspect ratio
	*/
	void setPerspective(GLfloat nearClip, GLfloat farClip, GLfloat fovy, GLfloat aspect);

	/*  Set the camera's location.
	*/
	void setCenter(const vec3 &center);

	//rotate so that the camera is facing the 'direction'
	void faceDirection(const vec3& direction);

	//return a unit vector pointing in the camera's current direction.
	//a rotation of 0 along every axis defines a direction (0,0,-1)
	vec3 getDirection() const;

	/*  Reset camera paramters.  The shader matrix location set with useShaderMatrix
	*   will not be reset, so a second call to useShaderMatrix is unnecessary.
	*
	*   The arguments are the same as the constructor.
	*/
	void reset(const vec3& center, GLfloat roll, GLfloat yaw, GLfloat pitch, 
			 GLfloat nearClip, GLfloat farClip, GLfloat fovy, GLfloat aspect);

	
	/*  Rotate camera yaw.
	*  @param degrees: amount to rotate by
	*/
	void rotateYaw(GLfloat degrees);

	/*  Rotate camera pitch.
	*  @param degrees: amount to rotate by
	*/
	void rotatePitch(GLfloat degrees);

	// Set the yaw to degrees
	void setYaw(GLfloat degrees);

	// Set the pitch to degrees
	void setPitch(GLfloat degrees);

	GLfloat getPitch() const;

	GLfloat getYaw() const;

	/* Move in the direction the camera is facing
	*  @param s: the amount to move by
	*/
	void moveForward(GLfloat s);

	/*  Move laterally to the direction the camera is facing
	*  @param s: the amount to move by
	*/
	void moveLateral(GLfloat s);

	/*  Moves the camera along the y-axis
	*  @param y: the amount to move by
	*/
	void changeAltitude(GLfloat y);

	/*  Send the camera's matrix to the shader variable set in Camera3D::setShaderMatrix
	*   Example usage: 
	*			Camera cam(...);
	*			cam.useView();
	*			glDrawArrays(...);
	*/
	void useView() const;

	/*  return the camera's matrix
	*/
	mat4 getMatrix() const;

	mat4 getPerspective() const;

	/*  returns only the world-to-camera part of the camera's matrix.  In other words,
	*	there is no projection transformation in the matrix returned by this function.
	*/
	mat4 worldCameraMatrix() const;

private:
	//camera position
	vec3 m_center;
	mutable Quaternion m_rotation;

	GLuint s_worldCameraProjection;

	//perspective
	mat4 m_perspective;
};

#endif