#ifndef __CUBE_H__
#define __CUBE_H__

#include "Angel.h"
#include "Quaternion.h"
#include "Graphics.h"

typedef Angel::vec4  point4;
typedef Angel::vec4  color4;

/****************************************************************************************************************
*														Sphere
****************************************************************************************************************/

class SphereVData
{
public:
	SphereVData() {
		normals = NULL;
		points = NULL;
	}
	~SphereVData() {
		if (points)
			delete[] normals;
		if (normals)
			delete[] points;
	}

	GLuint vao;
	GLuint nVertices;
	vec3* normals;
	point4* points;
};

class Sphere : public Model
{
public:

	/*generates sphere vertices and normals, and packs the result into a SphereVData object.  The second parameter
subdivisions specifies the depth of recursion to be used in generating the sphere, with high numbers resulting
in more vertices.  If the parameter "AverageNormals" is true then "smooth" normals will be created.  To create
the smooth normals, instead of averaging with nearby normals we just use a unit vector pointing from the center
of the sphere to each vertex.  Smooth normals are used for Phong shading and Gouraud shading.
*/
	static GLuint genVertices(ModelData *mdata, GLuint NumTimesToSubdivide, bool AverageNormals);

//Constructors

	/*  Create a sphere at position (0,0,0)
	*/
	Sphere(const ModelData& mdata);

	/*  Create a sphere at position (x,y,z)
	*/
	Sphere(GLfloat x, GLfloat y, GLfloat z, const ModelData& mdata, const Material& material, GLuint ShadingType, GLfloat scale);

	virtual ~Sphere() {}

	vec3 getPosition() const;

	GLuint getNumVertices() const;

	Material getMaterial() const;

	GLuint getShadingType() const;
	
	mat4 worldTransform() const;

//Mutators

	/*  Set a Sphere's rotation, specified in degrees.
	*   @param xDegrees: rotation about the x-axis
	*	@param yDegrees: rotation about the y-axis
	*	@param zDegrees: rotation about the z-axis
	*/
	void setRotation(GLfloat xDegrees, GLfloat yDegrees, GLfloat zDegrees);

	/*  Rotate incrementally about an arbitrary axis
	*	@param degrees: the amount to rotate by.
	*	@param axis: the axis of rotation
	*/
	void incRotation(GLfloat degrees, const vec3 &axis);

	/*  Set a Sphere's color
	*/
	void setColor(const color4& color);

	void setLocation(GLfloat x, GLfloat y, GLfloat z);

//Drawing

	/*  Send a Sphere's model-world matrix and color data to the shader variables
	*	which were set by Sphere::loadGeometry, then render this sphere to the backbuffer.
	*/
	void draw() const;

private:
	GLfloat m_x, m_y, m_z;
	GLfloat m_scale;
	mutable Quaternion m_qRotation; 
	GLuint m_numVertices;
	GLuint m_vao;
	Material m_material;
	GLuint m_shadingType;

//static functions
	static void triangle( const point4& a, const point4& b, const point4& c,
							int &Index, point4 points[], vec3 normals[], bool AverageNormals );
	static point4 unit( const point4& p );
	static void divide_triangle( const point4& a, const point4& b,
						const point4& c, int count, int& Index, point4 points[], vec3 normals[], bool AverageNormals );
	static void tetrahedron( int count, int& Index, point4 points[], vec3 normals[], bool AverageNormals );
};

/****************************************************************************************************************
*                                          Planet
****************************************************************************************************************/

class Planet : public Sphere
{
public:
	Planet(const vec3& orbitPoint, GLfloat radius, GLfloat angularVelocity, GLfloat theta,
		   const ModelData& mdata, const Material& material, GLuint ShadingType, GLfloat scale);

	virtual ~Planet() {}

	GLfloat getVelocity() const { return m_velocity; }

	vec3 getOrbitPoint() const;

	void orbit(GLfloat degrees);

	void setOrbitPoint(const vec3& p);

private:
	vec3 m_orbitPoint;
	GLfloat m_radius;
	GLfloat m_velocity;
	vec4 m_direction;
};

#endif