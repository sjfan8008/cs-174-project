#ifndef WORLD_H
#define WORLD_H

#include "Angel.h"
#include "RigidBody.h"

//For the purpose of testing, world::startSimulation will be called with these parameters
//when the player presses the 's' key.  Change them to whatever you want.
const vec3 TestForce = vec3(-0.00, -.205);
const int TestBall = 1;

//Intersection Tests
static bool spherePlaneIntersect(const Ball& sphere, const BoundingRectangle& rect);
static bool sphereIntersect(const Ball& s1, const Ball& s2);

/************************************************************************/
//						Bounding Rectangle
/************************************************************************/

class BoundingRectangle {

public:
	
	BoundingRectangle() {}
	//p1 through p4 are the four vertices of the rectangle, starting from top left
	//and going in a counter-clockwise order.  The normal of the rectangle's surface
	//will point in the direction of (p2-p1) CROSS (p4-p1).
	BoundingRectangle(const vec3& p1, const vec3& p2, const vec3& p3, const vec3& p4) 
	{		
		m_normal = normalize(cross((p2-p1),(p4-p1)));
		d = dot(m_normal, p1);
		m_leftPoint = p1;
		m_rightPoint = p3;
	}
	GLfloat get_d() {return d;}
	vec3 getNormal() { return m_normal;}
	vec3 getInnerLeftBound() {return m_leftPoint;}
	vec3 getInnerRightBound() {return m_rightPoint;}


private:
	vec3 m_leftPoint; 
	vec3 m_rightPoint;
	vec3 m_normal;
	GLfloat d;
};

/************************************************************************/

struct collision {
	collision() {};
	collision(Ball* a, Ball* b, GLfloat c) {
		b1 = a;
		b2 = b;
		d = c;
	}
	Ball* b1;
	Ball* b2;
	GLfloat d;
};

class World 
{
public:

	World();

	~World();

	//add a table to the world
	void addTable(Table* table);

	//Add a ball to the world
	void addBall(Ball* ball); 

	//Player strikes the ball!!
	//Apply 'force' to 'ballToStrike' and set it's velocity accordingly
	//'ballToStrike' is the index of the ball to apply the force to.
	//You will probably want to treat this initial force more like
	//an elastice collision, so pick a small time delta and calculate an impulse
	//from the force to get your change in momentum.
	void startSimulation(int ballToStrike, const vec3& force); 

	//Update all ball positions and velocities.
	int runSimulation(GLfloat timeDiff);

	//Returns the number of balls which are not in any hole.
	int numActiveBalls() const;

	//Returns true if and only if the cue ball is in a hole
	bool cueIsInHole() const;

	//Returns true if and only if the ball with index i is in the hole
	bool ballIsInHole(int i) const;

	//Return a pointer to the ball 'i'.  If i is not a valid
	//index, return NULL.
	const Ball* getBall(int i) const;

private:

	Ball *m_balls[16];
	Table *m_table;
	int m_numBalls;
	GLfloat TABLE_FRICTION;
	GLfloat WALL_FRICTION;	
	BoundingRectangle pocket_lb;
	BoundingRectangle pocket_lm;
	BoundingRectangle pocket_lt;
	BoundingRectangle pocket_rb;
	BoundingRectangle pocket_rm;
	BoundingRectangle pocket_rt;
	BoundingRectangle leftWall;
	BoundingRectangle rightWall;
	BoundingRectangle topWall;
	BoundingRectangle botWall;

	//Forbid copying and assignment of the world
	World(const World& other) {}
	World& operator=(const World& other) {}
	void executeBallWallCollision(int nBall);
	bool executeBallPockCollision(int nBall);
	//void updateBallPosition(int nBall);
	void updatePosition(GLfloat timediff);
	void checkBallToBallCollision();
	void doCollision(Ball* b1, Ball* b2);
	void orderCollisions(vector<collision> &c);
	void separateBalls(Ball* b1, Ball* b2);
	GLfloat getDistance(Ball* b1, Ball* b2);


};

#endif