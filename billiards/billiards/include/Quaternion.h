#ifndef QUATERNION_H
#define QUATERNION_H

#include "Angel.h"

const GLfloat Q_NORMAL_TOLERANCE = .00001;
const GLfloat Q_ZERO_TOLERANCE = .00001;

class Quaternion
{
public:

	/*  Constructor:  Construct a normalized quaternion.
	*	@param w, x, y, z: represents a Quaternion q = w + xi + yj + zk
	*		where w is the real part and x,y,z are the imaginary parts.
	*/
	Quaternion(GLfloat W, GLfloat X, GLfloat Y, GLfloat Z);

	/*  Construct a quaternion that represents a rotation about an arbitrary axis.
	*	@param axis: the axis of rotation.
	*	@param angle: the angle of rotation, in degrees.
	*	@param APPROX:  if true, will approximate sines and cosines.
	*/
	Quaternion(const vec3 &axis, GLfloat angle, bool APPROX = false);

	/*  Construct a quaternion that represents a rotation of the vector 
	*	(0,0,-1), such that it points in 'direction'
	*/
	Quaternion(const vec3& direction);

	/*  Multiply a quaternion by a vector, resulting in rotation of the vector.
	*	@param v: the vector to multiply
	*	@returns: a vector rotated by whatever rotation this quaternion represents
	*/
	vec3 operator*(const vec3 &v);
	vec4 operator*(const vec4 &v);

	/*  Multiple two quaternions together
	*/
	Quaternion operator*(const Quaternion &q2);

	/*  Return the conjugate of this quaternion
	*/
	Quaternion getConjugate();

	/* Negate this quaternion.
	*/
	void negate();

	/*  Normalize this quaternion.
	*/
	void normalize();

	/*  get a the equivalent rotation matrix to this quaternion
	*/
	mat4 getMatrix();

private:
	GLfloat w, x, y, z;
};

#endif