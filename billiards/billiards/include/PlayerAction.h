#ifndef PLAYERACTION_H
#define PLAYERACTION_H

#include "Camera.h"
#include "Input.h"

class Ball;
class Stick;
class Camera;

#define ACTION_INCOMPLETE 0
#define TOOK_SHOT 1
#define SHOT_CANCELLED 2
#define TOLERANCE 0.0001

const static GLfloat camInitDistance = 30.0;
const static GLfloat camMinDistance = 5.0;
const static GLfloat camMaxDistance = 50.0;
const static GLfloat camInitPitch = -30.0;
const static GLfloat camAngleSpeed = 30.0;   //degrees per second
const static GLfloat stickSpeed = 6.0;		 //units per second
const static GLfloat stickPitch = -10.0;
const static GLfloat stickMinDistance = 1.0;
const static GLfloat stickMaxDistance = 11.0;
const static GLfloat stickMinForce = 0.1;
const static GLfloat stickMaxForce = 1.5;
const static GLfloat shotAnimationTime = .10;

class PlayerAction
{
public:

	PlayerAction();

	~PlayerAction();

	/*  When this function is called, three things must happen.
	*
	*	First: 'cam' must be moved so that it is facing 'ball', looking down
	*	at an angle of camInitPitch, and is a distance of camInitDistance from
	*	the ball's position.  The new azimuthal angle of the camera should be the closest
	*	angle to the camera's current azimuth such that it is facing the ball.
	*
	*	Second: 'stick' must be pointing into the screen such that it meets the following conditions:
	*		condition 1: The tip of the stick is stickMinDistance units from the surface of 'ball'
	*		condition 2: The azimuth of the stick is the same as the azimuth of the camera
	*		condition 3: The pitch of the stick is stickPitch
	*
	*   Third: A subsequent call to getShotForce() must return a vector in the direction of 'stick', and
	*		the magnitude of this vector must be stickMinForce.
	*/
	void takeShotInit(const Ball& ball, Stick& stick, Camera3D& cam);

	/*  Let the player do one of four things: rotate the camera around 'ball', adjust their shot force,
	*	shoot the ball, or cancel their shot.  If the player shoots the ball, return TOOK_SHOT.  If the
	*	player cancel's their shot, return SHOT_CANCELLED.  In all other cases, return ACTION_INCOMPLETE.
	*	'timeDiff' shall be the difference (in seconds) between the last time takeShot was called.  'keystate'
	*	can be used to query the state of the keyboard device.  If getShotForce() is called after this function,
	*	it must return the new force the player has chosen.
	*/
	int takeShot(GLfloat timediff, const KeyboardState& keystate, const Ball& ball, Stick& stick, Camera3D& cam);

	/*  Return a vector which represents the current force of the player's shot.  
	*/
	const vec3* getShotForce() const;

private:
	PlayerAction(const PlayerAction& other);
	PlayerAction& operator=(const PlayerAction& other);

	int animateStick(Stick& stick, const Ball& ball, GLfloat timeDiff);

	vec3 m_force;
	GLfloat m_camToBallDistance;
	GLfloat m_stickToBallDistance;
	GLfloat m_stickVelocity;
	bool m_shotInProgress;
};

#endif