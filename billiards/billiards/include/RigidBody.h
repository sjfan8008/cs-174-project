#ifndef BALL_H
#define BALL_H

#include "Graphics.h"

enum { CUE, BALL1, BALL2, BALL3, BALL4, BALL5, BALL6, BALL7, BALL8, BALL9, BALL10, BALL11, BALL12, BALL13, BALL14, BALL15 };

int iPow(int,int);

class Ball : public Model
{
public:

	Ball();

	Ball(vec3 pos, GLfloat vel, GLfloat mass, GLfloat angle, GLfloat radius, int type, 
		 BufferManager& bufman, const Texture& texture, const Material& material, GLuint shadingType);

	Ball(vec3 pos, GLfloat vel, GLfloat mass, GLfloat angle, GLfloat radius, int type);

	Ball(const Ball& other);

	virtual ~Ball();

	Ball& operator=(const Ball& other);

	virtual int getBufferDataIndex() const;

	//Return the ball's type, eg. CUE, BALL1, BALL2 etc...
	int getType() const;

	//returns the ball's mass, in Kilograms
	GLfloat getMass() const;

	//return the ball's velocity
	GLfloat getVelocity() const;

	GLfloat getRadius() const { return m_radius; }

	//sets velocity to v
	void setVelocity(GLfloat v);

	//return the angle at which the ball is moving
	GLfloat getAngle() const;

	//sets angle to a
	void setAngle(GLfloat a);

	//returns true if ball is in a pocket
	bool inPocket() const;

	//changes pocket to true or false
	void setPocket(bool b);

	//draws ball
	virtual void draw() const;

private:
	GLfloat m_velocity;
	GLfloat m_mass;
	GLfloat m_angle;
	bool m_pocket;
	int m_type;
	GLfloat m_radius;

	static int bufferDataIndex;

	//functions for generating buffer data
	static BufferData createBallBufferData();
	static void triangle( const point4& a, const point4& b, const point4& c, int &Index, BufferData& mdata, bool AverageNormals );
	static point4 unit( const point4& p );
	static void divide_triangle( const point4& a, const point4& b, const point4& c, int count, int& Index, BufferData& mdata, bool AverageNormals );
	static void tetrahedron( int count, int& Index, BufferData& mdata, bool AverageNormals );
};

/*****************************************************************/

class Tetrahedron : public Model
{
public:

	static void genVertices(BufferData* mdata, const point4 vertices[8]);

	//Create a tetrahedron with vertices p1, p2, p3, p4, p5, p6, p7, and p8.
	Tetrahedron(BufferManager& bufMan, const Texture& text, const Material& material, const point4 vertices[8]);
	virtual ~Tetrahedron();
	Tetrahedron(const Tetrahedron& other);
	Tetrahedron& operator=(const Tetrahedron& other);

	virtual int getBufferDataIndex() const { return m_bufferIndex; }

	//get the plane describing one of the tetrahedron's 8 faces
	//'i' is an integer between 0 and 7 specifying which face.
	const BoundingRectangle* getFace(int i) const;
	
	//draw Tetrahedron
	virtual void draw() const;

private:
	int m_bufferIndex;
	BoundingRectangle* m_p[6];
};

/*****************************************************************/

class Stick : public Model
{
public:

	Stick(BufferManager& bufMan, const Texture& text, const Material& material, GLfloat frontRadius, GLfloat backRadius, GLfloat length);

	virtual ~Stick() {};
	Stick(const Stick& other) {};
	Stick& operator=(const Stick& other) {};

	virtual int getBufferDataIndex() const { return m_bufferIndex; }

	virtual void draw() const { Model::draw(); }

private:
	static int m_bufferIndex;
	static void genVertices(BufferData* bdata, GLfloat frontRadius, GLfloat backRadius, GLfloat length, int numQuads);
	static void createCircle(point4 buffer[], GLfloat radius, GLfloat zcoord, int numPoints);
	static void createSideVertices(BufferData* bdata, int& Index, point4 frontCircle[], point4 backCircle[], int numQuads);
	static void createEndVertices(BufferData* bdata, int& Index, point4 circlePoints[], int zcoord, int numCirclePoints);
	static void sideVertex(BufferData* bdata, int& Index, const point4& p);
	static void endVertex(BufferData* bdata, int& Index, const point4& p, const vec3& normal);
};

/*****************************************************************/

enum Pocket {POCKET_LEFT_BOTTOM=0, POCKET_LEFT_MIDDLE, POCKET_LEFT_TOP, POCKET_RIGHT_BOTTOM, POCKET_RIGHT_MIDDLE, POCKET_RIGHT_TOP};
enum Cusion {LEFT_CUSION, RIGHT_CUSION, BOTTOM_CUSION, TOP_CUSION};

struct Line {
	vec2 start;
	vec2 end;
};

class Table : public Movable
{
public:

	Table(BufferManager& bufMan);
	virtual ~Table();

	int numComponents() const;
	const Tetrahedron* getComponent(int i) const;

/*****************************************************/
//			Physics group modify these functions

	//Returns a Line which represents the boundary between
	//the pocket and the rest of the table.  If a ball passes
	//completely through such a line, it is considered to be
	//inside the pocket.
	BoundingRectangle getPocketBoundingPlane(Pocket pocket) const;

	//Returns a Plane which represents the surface of the 
	//cusion which is facing the table interior.
	BoundingRectangle getCusionPlane(Cusion cusion) const;

/*********************************************************/

private:
	//Don't want to worry about these
	Table(const Table& other);
	Table& operator=(const Table& other);

	/*
	Tetrahedron* m_leftCusion;
	Tetrahedron* m_leftSubCusion1;
	Tetrahedron* m_leftSubCusion2;

	Tetrahedron* m_rightCusion;
	Tetrahedron* m_rightSubCusion1;
	Tetrahedron* m_rightSubCusion2;

	Tetrahedron* m_bottomCusion;
	Tetrahedron* m_bottomSubCusion;

	Tetrahedron* m_topCusion;
	Tetrahedron* m_topSubCusion;

	Tetrahedron* m_surface;
	Tetrahedron* m_legs[4];
	*/
	static const int m_nComponents = 15;
	Tetrahedron* m_components[m_nComponents];

	point4 m_pocketRects[6][4];

	//each row is an array representing the four vertices of a rectangle
	point4 m_wallRects[4][4];
};

#endif