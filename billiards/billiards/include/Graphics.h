#ifndef GRAPHICS_H
#define GRAPHICS_H

#include "Angel.h"
#include "Quaternion.h"
#include "Lighting.h"
#include <vector>
using std::vector;

typedef vec4 point4;
typedef vec4 color4;

//Shading types
enum {FLAT, GOURAUD, PHONG};

#define TWO_PI 2*M_PI

class BoundingRectangle;

/*****************************************************************/
//							Movable
/*****************************************************************/

class Movable
{
public:

	Movable(GLfloat x, GLfloat y, GLfloat z, GLfloat scale, const Quaternion& rotation);
	virtual ~Movable();

	vec3 getPosition() const;

	void setPosition(GLfloat x, GLfloat y, GLfloat z);

	//rotate an angle of 'degrees' around 'axis'
	void rotate(const vec3& axis, GLfloat degrees);

	// Set the yaw to degrees
	void setYaw(GLfloat degrees);

	// Set the pitch to degrees
	void setPitch(GLfloat degrees);

	//rotate so that the model is facing the 'direction'
	void faceDirection(const vec3& direction);

	//return a unit vector pointing in the model's current direction.
	//a rotation of 0 along every axis defines a direction (0,0,-1)
	vec3 getDirection() const;

	//change the position to (x + dx, y + dy, z + dz)
	void translate(GLfloat dx, GLfloat dy, GLfloat dz);

	//Move in the direction the Movable is facing.  A Movable
	//with 0 rotation faces the -z axis.
	void moveParallelToCurrentDirection(GLfloat s);

	//set the model scale to 's'
	void scale(GLfloat s);

	GLfloat getScale() const { return m_scale; }

	//returns the model-world matrix
	mat4 worldTransform() const;

private:

	GLfloat m_x;
	GLfloat m_y;
	GLfloat m_z;
	GLfloat m_scale;
	mutable Quaternion m_rotation;
};


/*****************************************************************/
//							Material
/*****************************************************************/

typedef struct {

	color4 ambient;
	color4 diffuse;
	color4 specular;
	GLfloat shininess;

} Material;

/*****************************************************************/
//							ModelData
/*****************************************************************/
class BufferManager;

struct ModelData
{
public:
	ModelData() {
		normals = NULL;
		points = NULL;
		textCoords = NULL;
	}
	~ModelData() {
		if (points)
			delete[] normals;
		if (normals)
			delete[] points;
		if (textCoords)
			delete[] textCoords;
	}

	const BufferManager* bufMan;
	GLuint nVertices;
	GLuint texture;
	vec3* normals;
	point4* points;
	vec2* textCoords;
	Material material;
	GLuint shadingType;
};

/*****************************************************************/
//							Model
/*****************************************************************/

class Model : public Movable
{
public:
	Model();

	Model(const ModelData& mdata);

	void Init(const ModelData& mdata);

	virtual ~Model();

	Model(const Model& other);

	virtual Model& operator=(const Model& other);

	virtual void draw() const;

	virtual int getBufferDataIndex() const = 0;

	Material getMaterial() const;

	GLuint getShadingType() const;

private:
	bool m_init;

//Graphics data
	const BufferManager* m_bufMan;
	GLuint m_texture;
	Material m_material;
	GLuint m_shadingType;
};

/*****************************************************************/
//							Shader
/*****************************************************************/

class Shader
{
public:

	Shader(GLuint program);

	void setModelView(const mat4& mat);
	void setProjection(const mat4& mat);
	void setCam(const mat4& mat);
	void setLighting(const LightSource* light, const Material& material);
	void setShadingType(GLuint shadingType);
	void setColor(const GLubyte color[3]);
	void setRenderMode(int mode);

	GLuint getVertexSize() { return sizeof(point4) + sizeof(vec3); }

	GLuint getNormalLoc() const { return m_normal; }
	GLuint getPosLoc() const { return m_vPosition; }
	GLuint getTextureLoc() const { return m_vTexCoord; }

private:

	//vertex arrays
	GLuint m_vPosition;
	GLuint m_normal;
	GLuint m_vTexCoord;

	//transformations
	GLuint m_ModelView;
	GLuint m_Projection;
	GLuint m_Camera;

	//lighting
	GLuint m_ShadingType;
	GLuint m_lightPosition;
	GLuint m_AmbientProduct, m_SpecularProduct, m_DiffuseProduct, m_Shininess;

	GLuint m_renderMode;
	GLuint m_pickColor;
};

/*****************************************************************/
//						BufferData
/**************************************************************/

class BufferData
{
public:

	BufferData();

	BufferData(const ModelData& md) {

		points = new point4[md.nVertices];
		normals = new vec3[md.nVertices];
		textCoords = new vec2[md.nVertices];
		nVertices = md.nVertices;
		for (GLuint i = 0; i < md.nVertices; i++) {
			points[i] = md.points[i];
			normals[i] = md.normals[i];
			textCoords[i] = md.textCoords[i];
		}
	}

	BufferData(const BufferData& other) {
		points = new point4[other.nVertices];
		normals = new vec3[other.nVertices];
		textCoords = new vec2[other.nVertices];
		nVertices = other.nVertices;
		for (GLuint i = 0; i < nVertices; i++) {
			points[i] = other.points[i];
			normals[i] = other.normals[i];
			textCoords[i] = other.textCoords[i];
		}
	}

	BufferData& operator=(const BufferData& other) {
		if (points)
			delete[] points;
		if (normals)
			delete[] normals;
		if (textCoords)
			delete[] textCoords;

		points = new point4[other.nVertices];
		normals = new vec3[other.nVertices];
		textCoords = new vec2[other.nVertices];
		nVertices = other.nVertices;

		for (GLuint i = 0; i < nVertices; i++) {
			points[i] = other.points[i];
			normals[i] = other.normals[i];
			textCoords[i] = other.textCoords[i];
		}
		return *this;
	}

	GLuint getDataSize() const {
		return nVertices * (sizeof(point4) + sizeof(vec3) + sizeof(vec2));
	}

	point4* points;
	vec3* normals;
	vec2* textCoords;
	GLuint nVertices;
	GLuint vao;
};

/**************************************************************/
//						Buffer Manager
/**************************************************************/

#define BUF_DATA_UNINIT -1

class BufferManager
{
public:

	BufferManager(const Shader* pShader);

	//bufferData == UNINITIALZED should always return false
	bool isLoaded(GLuint index) const;

	int loadBufferData(const BufferData& g);

	void createGLBuffers();

	void bindBuffer() const;

	void freeCPUmemory();

	void unload();

	GLuint getVertexAttributeObject(GLuint index) const;

	GLuint getNumVertices(GLuint index) const;

private:
	const Shader* m_shader;
	GLuint m_buffer;
	vector<BufferData> m_data;
	GLuint m_nData;
	GLuint m_bufferSize;
};

class Texture
{
public:
	Texture(const char filename[]);

	GLuint width() const;
	GLuint height() const;
	GLuint getGLtextObject() const;

private:
	GLuint m_texture;
	GLuint m_width;
	GLuint m_height;
};

/**************************************************************/
//						PickList
/**************************************************************/

class PickList
{
public:

private:


};

#endif