//
//  main.cpp
//  sphere
//
//  Created by Scott Friedman on 10/10/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#include <iostream>
#include "Angel.h"
#include "Camera.h"
#include "Input.h"
#include "Timer.h"
#include "Lighting.h"
#include "World.h"
#include "PlayerAction.h"
#include "IL\ilut.h"

void init( void );
void display( void );
void keyboard( unsigned char key, int x, int y );
void reshape( int width, int height );

//----

typedef vec4 point4;
typedef vec4 color4;

//capture the current and previous keyboard state.
KeyboardState keyState;
KeyboardState prevKeyState;

#define RENDER 1
#define SELECT 2
int mode = RENDER;
int cursorX,cursorY;

enum State {SURVEY_TABLE, TAKE_SHOT, SIMULATION_RUNNING, STATE_EXIT};
State state = SURVEY_TABLE;

//capture the time between successive calls to idle function
Timer timer;
GLfloat lastTime;

Shader* shader;
BufferManager* bufMan;

World* world;

//camera
Camera3D* camera = NULL;
const vec3 camInitPos(25,30,60);
const GLfloat camInitTheta = 30;
const GLfloat camInitPhi = -40;
const GLfloat zNear = -.2;
const GLfloat zFar = 50;
const GLfloat fovy = 60;
const GLfloat camInitAspect = 1.33;
GLfloat aspect = camInitAspect;

const int NumBalls = 16;
const int BallRadius = 1.0;
Ball* spheres[NumBalls];
GLubyte pickColors[NumBalls][3] = { {0,0,1}, {0,0,2}, {0,0,3}, {0,0,4}, {0,0,5}, {0,0,6}, {0,0,7}, {0,0,8}, {0,0,9}, {0,0,10}, {0,0,11}, {0,0,12}, {0,0,13}, {0,0,14}, {0,0,15}, {0,0,16}};
Ball* pickedBall = NULL;

Stick* stick;
LightSource* light;
PlayerAction* playerAction;
Table* pTable;


Ball* getBallFromColor(GLubyte pixel[3]) {

	//The third pixel should be the 1+index of the ball
	GLubyte i = pixel[2];
	if (1 <= i && i <= NumBalls) {
		return spheres[i-1];
	}
	else {
		return NULL;
	}
}

void init( void )
{


	// Load shaders and use the resulting shader program
    GLuint program = InitShader( "Shaders/vshader.glsl", "Shaders/fshader.glsl" );
    glUseProgram( program );
	shader = new Shader(program);
	
	world = new World();
	Material SmoothMaterial = { color4( 0.0f, 0.0f, 0.1f, 1.0f ),
							 color4( 0.5f, 0.5f, 0.7f, 1.0f ),
							 color4( .5f, 0.5f, 0.5f, 1.0f ),
							 30.0f};


	//TODO: move this somewhere better
	glUniform1i( glGetUniformLocation(program, "texture"), 0 );
	glActiveTexture( GL_TEXTURE0 );

/*** object initialization ***/

	bufMan = new BufferManager(shader);
	Texture texture0("Textures/ball0.jpg");
	Texture texture1("Textures/ball1.jpg");
	Texture texture2("Textures/ball2.jpg");
	Texture texture3("Textures/ball3.jpg");
	Texture texture4("Textures/ball4.jpg");
	Texture texture5("Textures/ball5.jpg");
	Texture texture6("Textures/ball6.jpg");
	Texture texture7("Textures/ball7.jpg");
	Texture texture8("Textures/ball8.jpg");
	Texture texture9("Textures/ball9.jpg");
	Texture texture10("Textures/ball10.jpg");
	Texture texture11("Textures/ball11.jpg");
	Texture texture12("Textures/ball12.jpg");
	Texture texture13("Textures/ball13.jpg");
	Texture texture14("Textures/ball14.jpg");
	Texture texture15("Textures/ball15.jpg");
	


	//balls
	spheres[0] = new Ball(vec3(0,0,8), 0.0, 1.0, 0.0, BallRadius, CUE, *bufMan, texture0, SmoothMaterial, PHONG);
	world->addBall(spheres[0]);

	spheres[1] = new Ball(vec3(0,0,0), 0.0, 1.0, 0.0, BallRadius, BALL1, *bufMan, texture1, SmoothMaterial, PHONG);
	world->addBall(spheres[1]);

	spheres[2] = new Ball(vec3(4.1,0,-7.0), 0.0, 1.0, 0.0, BallRadius, BALL2, *bufMan, texture2, SmoothMaterial, PHONG);
	world->addBall(spheres[2]);

	spheres[3] = new Ball(vec3(-4.1, 0, -7.0), 0.0, 1.0, 0.0, BallRadius, BALL3, *bufMan, texture3, SmoothMaterial, PHONG);
	world->addBall(spheres[3]);

	spheres[4] = new Ball(vec3(-2.05, 0, -3.5), 0.0, 1.0, 0.0, BallRadius, BALL4, *bufMan, texture4, SmoothMaterial, PHONG);
	world->addBall(spheres[4]);

	spheres[5] = new Ball(vec3(2.05,0,-3.5), 0.0, 1.0, 0.0, BallRadius, BALL5, *bufMan, texture5, SmoothMaterial, PHONG);
	world->addBall(spheres[5]);

	spheres[6] = new Ball(vec3(1.025, 0, -5.25), 0.0, 1.0, 0.0, BallRadius, BALL6, *bufMan, texture6, SmoothMaterial, PHONG);
	world->addBall(spheres[6]);

	spheres[7] = new Ball(vec3(0, 0, -7), 0.0, 1.0, 0.0, BallRadius, BALL7, *bufMan, texture7, SmoothMaterial, PHONG);
	world->addBall(spheres[7]);

	spheres[8] = new Ball(vec3(0, 0, -3.5), 0.0, 1.0, 0.0, BallRadius, BALL8, *bufMan, texture8, SmoothMaterial, PHONG);
	world->addBall(spheres[8]);

	spheres[9] = new Ball(vec3(-1.025,0,-5.25), 0.0, 1.0, 0.0, BallRadius, BALL9, *bufMan, texture9, SmoothMaterial, PHONG);
	world->addBall(spheres[9]);

	spheres[10] = new Ball(vec3(-1.025, 0, -1.75), 0.0, 1.0, 0.0, BallRadius, BALL10, *bufMan, texture10, SmoothMaterial, PHONG);
	world->addBall(spheres[10]);

	spheres[11] = new Ball(vec3(1.025, 0, -1.75), 0.0, 1.0, 0.0, BallRadius, BALL11, *bufMan, texture11, SmoothMaterial, PHONG);
	world->addBall(spheres[11]);

	spheres[12] = new Ball(vec3(3.075, 0, -5.25), 0.0, 1.0, 0.0, BallRadius, BALL12, *bufMan, texture12, SmoothMaterial, PHONG);
	world->addBall(spheres[12]);

	spheres[13] = new Ball(vec3(2.05, 0, -7), 0.0, 1.0, 0.0, BallRadius, BALL13, *bufMan, texture13, SmoothMaterial, PHONG);
	world->addBall(spheres[13]);

	spheres[14] = new Ball(vec3(-2.05, 0, -7), 0.0, 1.0, 0.0, BallRadius, BALL14, *bufMan, texture14, SmoothMaterial, PHONG);
	world->addBall(spheres[14]);

	spheres[15] = new Ball(vec3(-3.075, 0, -5.25), 0.0, 1.0, 0.0, BallRadius, BALL15, *bufMan, texture15, SmoothMaterial, PHONG);
	world->addBall(spheres[15]);
	//table
	pTable = new Table(*bufMan);
	pTable->scale(2.0);
	pTable->setPosition(0, -BallRadius, 0);
	world->addTable(pTable);

	//create stick
	stick = new Stick(*bufMan, Texture("Textures/wood.jpg"), SmoothMaterial, 0.25, 0.40, 30.0);
	stick->setPitch(-10.0);
	stick->setPosition(0, 0, 10);

	//send buffers to gpu
	bufMan->createGLBuffers();	
	//free vertex arrays from cpu memory
	bufMan->freeCPUmemory();

/*****************************/


	//create camera
	camera = new Camera3D(camInitPos, 0, camInitTheta, camInitPhi, zNear, zFar, fovy, camInitAspect);

	//get lighting shader uniforms and initialize a new light source
    color4 light_ambient(   1.f, 1.f, 1.f, 1.0f );
    color4 light_diffuse(   1.f, 1.f, 1.f, 1.0f );
    color4 light_specular(  1.f, 1.f, 1.f, 1.0f );
	vec3 LightPosition(1,20,3);
	light = new LightSource(LightPosition, light_specular, light_diffuse, light_ambient);

	playerAction = new PlayerAction();

    glEnable( GL_DEPTH_TEST );
    glClearColor(0, 0, 0, 0);
}

//-------------------------------------------------------------------------------------

void processPick(GLubyte pixel[3])
{
	GLint viewport[4];

	glGetIntegerv(GL_VIEWPORT,viewport);

	glReadPixels(cursorX,viewport[3]-cursorY,1,1,GL_RGB,GL_UNSIGNED_BYTE,(void *)pixel);

	//printf("%d %d %d\n",pixel[0],pixel[1],pixel[2]);
}

//-------------------------------------------------------------------------------------

void render() {

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//need this to transform light position from world coordinates to camera coordinates
    mat4 cam = camera->getMatrix();
	shader->setCam(cam);

	mat4 projection = camera->getPerspective();
	shader->setProjection(projection);

	//apply lighting
	shader->setRenderMode(RENDER);

	for (int i = 0; i < NumBalls; i++)
	{
		//model-view transformation
		mat4 model_view = cam * spheres[i]->worldTransform();
		shader->setModelView(model_view);

		//lighting
		shader->setLighting(light, spheres[i]->getMaterial());
		shader->setShadingType(spheres[i]->getShadingType());

		//draw arrays
		spheres[i]->draw();
	}

	//Draw table, one component at a time
	int numComponents = pTable->numComponents();
	mat4 tableModelWorldMatrix = pTable->worldTransform();
	for (int i = 0; i < numComponents; i++) {
		
		const Tetrahedron* component = pTable->getComponent(i);
		if (!component)
			continue;

		mat4 model_view = cam * tableModelWorldMatrix * component->worldTransform();
		shader->setModelView(model_view);

		//apply lighting
		shader->setLighting(light, component->getMaterial());
		shader->setShadingType(component->getShadingType());

		//draw arrays
		component->draw();
	}

	if (state == SURVEY_TABLE) {
	glEnable( GL_BLEND );
	glDepthMask( GL_FALSE );
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}

	//Draw stick
	mat4 model_view = cam* stick->worldTransform();
	shader->setModelView(model_view);
	shader->setLighting(light, stick->getMaterial());
	shader->setShadingType(stick->getShadingType());
	stick->draw();

	glDepthMask( GL_TRUE );
	glDisable( GL_BLEND );

	glutSwapBuffers();
}

//-------------------------------------------------------------------------------------

void renderPicking() {

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	shader->setRenderMode(SELECT);

	//need this to transform light position from world coordinates to camera coordinates
    mat4 cam = camera->getMatrix();
	shader->setCam(cam);

	mat4 projection = camera->getPerspective();
	shader->setProjection(projection);

	for (int i = 0; i < NumBalls; i++)
	{
		//model-view transformation
		mat4 model_view = cam * spheres[i]->worldTransform();
		shader->setModelView(model_view);

		//Color for picking
		shader->setColor(pickColors[i]);

		//draw arrays
		spheres[i]->draw();
	}

	//select a ball
	GLubyte pixel[3];
	processPick(pixel);
	pickedBall = getBallFromColor(pixel);
}

//-------------------------------------------------------------------------------------

void display( void )
{
	if (mode == RENDER) {
		render();
	}
	else {
		renderPicking();
		mode = RENDER;
	}
}

//-------------------------------------------------------------------------------------

void reshape( int width, int height )
{
    glViewport(0, 0, width, height);
    
    GLfloat left = -2.0f, right = 2.0f;
    GLfloat top = 2.0f, bottom = -2.0f;
    GLfloat zNear = 1.0f, zFar = 20.0f;
    
    GLfloat aspect = GLfloat(width)/height;
    
    if ( aspect > 1.0f )
    {
        left *= aspect;
        right *= aspect;
    }
    else
    {
        top /= aspect;
        bottom /= aspect;
    }
    
    mat4 projection = Ortho(left, right, bottom, top, zNear, zFar);
    //mat4 projection = Perspective(70.0f, aspect, zNear, zFar);
	shader->setProjection(projection);
}

//----------------------------------------------------------------------------

State
handleInput(GLfloat timeDiff)
{
	State newState = SURVEY_TABLE;

	//exit
	if ((keyState.isDown('q') && !prevKeyState.isDown('q')) 
		|| keyState.isDown('Q') || keyState.isDown(033))
	{
	    return STATE_EXIT;
	}

	//Camera movement
	const GLfloat UnitsPerSecond = 20;
	const GLfloat DegreesPerSecond = 50;

	if (keyState.isDown('a'))
		camera->moveLateral(-UnitsPerSecond*timeDiff);
	if (keyState.isDown('d'))
		camera->moveLateral(UnitsPerSecond*timeDiff);
	if (keyState.isDown('w'))
		camera->moveForward(UnitsPerSecond*timeDiff);
	if (keyState.isDown('s'))
		camera->moveForward(-UnitsPerSecond*timeDiff);
	if (keyState.isDown('u'))
		camera->changeAltitude(UnitsPerSecond*timeDiff);
	if (keyState.isDown('j'))
		camera->changeAltitude(-UnitsPerSecond*timeDiff);

	//camera rotation
	if (keyState.isDown(KEY_UP))
		camera->rotatePitch(DegreesPerSecond*timeDiff);
	if (keyState.isDown(KEY_DOWN))
		camera->rotatePitch(-DegreesPerSecond*timeDiff);
	if (keyState.isDown(KEY_LEFT))
		camera->rotateYaw(DegreesPerSecond*timeDiff);
	if (keyState.isDown(KEY_RIGHT))
		camera->rotateYaw(-DegreesPerSecond*timeDiff);

	//camera reset
	if (keyState.isDown('r'))
	{
		camera->reset(camInitPos, 0, camInitTheta, camInitPhi, zNear, zFar, fovy, camInitAspect);
	}

	return newState;
}

//----------------------------------------------------------------------------

void
update( int value )
{

	GLfloat currentTime = timer.GetElapsedTime();
	GLfloat timeDiff = currentTime - lastTime;
	lastTime = currentTime;

	int status;
	const vec3* force;

	switch(state)
	{
	case SURVEY_TABLE:

		if (pickedBall) {
			playerAction->takeShotInit( *pickedBall, *stick, *camera );
			state = TAKE_SHOT;
		}
		else {
			state = handleInput(timeDiff);
		}

	break;

	case TAKE_SHOT:

		status = playerAction->takeShot( timeDiff, keyState, *pickedBall, *stick, *camera );

		if (status == TOOK_SHOT) {

			force = playerAction->getShotForce();
			if (!force) {
				printf("Error: invalid force\n");
				exit(1);
			}
			world->startSimulation( pickedBall->getType(), *force );
			state = SIMULATION_RUNNING;
			pickedBall = NULL;
		}
		else if (status == SHOT_CANCELLED) {
			state = SURVEY_TABLE;
			pickedBall = NULL;
		}

	break;

	case SIMULATION_RUNNING:

		if ( ! world->runSimulation(timeDiff) )
			state = SURVEY_TABLE;

	break;

	case STATE_EXIT:

		//clean up
		for (int i=0; i<NumBalls; i++)
			delete spheres[i];
		delete pTable;
		delete stick;
		delete camera;
		delete light;
		delete shader;
		delete bufMan;
		delete world;
		delete playerAction;

		exit( EXIT_SUCCESS );

	break;
	}

	prevKeyState = keyState;
	glutPostRedisplay();
	glutTimerFunc(16, update, 0);
}

//----------------------------------------------------------------------------

void mouse(int button, int state, int x, int y) {



	if (button != GLUT_LEFT_BUTTON || state != GLUT_DOWN)
		return;

	//Only allow selection in the SURVEY_TABLE state
	if (::state != SURVEY_TABLE)
		return;

	cursorX = x;
	cursorY = y;
	mode = SELECT;
}

//grab and store the keyboard state in order to circumvent key repeat delay

void
keyboardDown( unsigned char key, int x, int y )
{
	keyState.setKey(key, true);
}

void
keyboardUp( unsigned char key, int x, int y )
{
	keyState.setKey(key, false);
}

void
keyboardSpecialDown( int key, int x, int y )
{
	keyState.setSpecialKey(key, true);
}

void
KeyboardSpecialUp( int key, int x, int y )
{
	keyState.setSpecialKey(key, false);
}

//----------------------------------------------------------------------------

int main (int argc, char *argv[] )
{
    glutInit( &argc, argv );
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowPosition(100, 50);
	glutInitWindowSize(800, 600);
    glutCreateWindow( "Billiards" );
    
#ifndef __APPLE__  // include Mac OS X verions of headers
	glewInit( );
#endif
	ilInit();

	init( );
    
    glutReshapeFunc(reshape);
	glutSetKeyRepeat(GLUT_KEY_REPEAT_ON);
    glutDisplayFunc( display );
	glutMouseFunc( mouse );
    glutKeyboardFunc( keyboardDown );
	glutKeyboardUpFunc( keyboardUp );
	glutSpecialFunc( keyboardSpecialDown );
	glutSpecialUpFunc( KeyboardSpecialUp );
    glutTimerFunc(30, update, 0);
    
    glutMainLoop();
    return 0;
}

