#include "World.h"
#include <iostream>

using namespace std;


World::World() {
	
	m_numBalls = 0;	
	TABLE_FRICTION = 0.003f;
	WALL_FRICTION  = 0.02f;
	
}

World::~World() {

}

void World::addTable(Table* table) {
	m_table = table;
	pocket_lb = m_table->getPocketBoundingPlane(POCKET_LEFT_BOTTOM);
	pocket_lm = m_table->getPocketBoundingPlane(POCKET_LEFT_MIDDLE); // left middle pocket
	pocket_lt = m_table->getPocketBoundingPlane(POCKET_LEFT_TOP);    // left top pocket
	pocket_rb = m_table->getPocketBoundingPlane(POCKET_RIGHT_BOTTOM); // right bottom pocket
	pocket_rm = m_table->getPocketBoundingPlane(POCKET_RIGHT_MIDDLE); 
	pocket_rt = m_table->getPocketBoundingPlane(POCKET_RIGHT_TOP);    // right top pocket

	leftWall = m_table->getCusionPlane(LEFT_CUSION);			// left wall
	rightWall = m_table->getCusionPlane(RIGHT_CUSION);		// right wall
	topWall = m_table->getCusionPlane(TOP_CUSION);			// top wall
	botWall = m_table->getCusionPlane(BOTTOM_CUSION);			// bottom wall
}

void World::addBall(Ball* ball) {
	m_balls[m_numBalls] = ball;
	m_numBalls++;
}

void World::startSimulation(int ballToStrike, const vec3& force) {
	m_balls[ballToStrike]->setVelocity(sqrt(pow((float)force.x, 2) + pow((float)force.z, 2)));
	
	if (force.x > 0.0f)
		m_balls[ballToStrike]->setAngle(atan(force.z/force.x) / DegreesToRadians );
	else if (force.x < 0.0f)
		m_balls[ballToStrike]->setAngle(atan(force.z/force.x) / DegreesToRadians + 180.0 );
	else if (force.x == 0.0f && force.z > 0.0f)
		m_balls[ballToStrike]->setAngle(90.0f);
	else
		m_balls[ballToStrike]->setAngle(270.0f);	
} 

int World::runSimulation(GLfloat timeDiff) {
	
	int count = 0;
	checkBallToBallCollision();
	for (int i = 0; i < m_numBalls; i++) 
	{		
		if (!(m_balls[i]->inPocket()) &&		// if ball had not already gone into the pocket
			!(executeBallPockCollision(i)))		// AND if ball did not go into the pocket in current frame
		{
			executeBallWallCollision(i);		// then check ball-wall collisions
		}
	}
	
	for (int i = 0; i < m_numBalls; i++) 
	{
		if (m_balls[i]->getVelocity() > 0.0)
			count++;
	}
	updatePosition(timeDiff);

	if (count == 0)
		return 0;
	else
		return 1;

	return 1;
}

int World::numActiveBalls() const {

	return 0;
}

bool World::cueIsInHole() const {

	return false;
}

const Ball* World::getBall(int i) const {

	return NULL;
}

void World::executeBallWallCollision(int nBall)
{
	GLfloat vel = m_balls[nBall]->getVelocity();
	if (vel <= 0)
		return;	
	vec3    pos = m_balls[nBall]->getPosition();
	GLfloat rad = m_balls[nBall]->getRadius();
	GLfloat ang = m_balls[nBall]->getAngle();
	
	
	vec3 normal_lw = leftWall.getNormal();
	GLfloat dist_lw = dot(pos, normal_lw) - leftWall.get_d();
	vec3  point_lw = leftWall.getInnerLeftBound();

	
	vec3 normal_rw = rightWall.getNormal();
	GLfloat dist_rw = dot(pos, normal_rw) - rightWall.get_d();
	vec3  point_rw = rightWall.getInnerLeftBound();

	
	vec3 normal_tw = topWall.getNormal();
	GLfloat dist_tw = dot(pos, normal_tw) - topWall.get_d();
	vec3  point_tw = topWall.getInnerLeftBound();

	
	vec3 normal_bw = botWall.getNormal();
	GLfloat dist_bw = dot(pos, normal_bw) - botWall.get_d();
	vec3  point_bw = botWall.getInnerLeftBound();
	
	// left or right cushion collision	
	if (abs(dist_lw) <= rad|| abs(dist_rw) <= rad) 
	{	
		// set the ball's position with its surface right at the cushion (to offset it going into the cushion)
		if (pos.x < 0) // colliding with left wall			
			m_balls[nBall]->setPosition(point_lw.x + rad, pos.y, pos.z); 
		else		   // colliding with left wall		
			m_balls[nBall]->setPosition(point_rw.x - rad, pos.y, pos.z); 
		// adjust ball's angle and velocity
		ang  = 270 - ang - 90;							// incident angle		
		vel -= WALL_FRICTION;							// reduce ball's velocity

		m_balls[nBall]->setAngle(ang); 		
		m_balls[nBall]->setVelocity(vel);
	}

	// top or bottom cushion collisions
	else if (abs(dist_bw) <= rad || abs(dist_tw) <= rad)
	{
		// set the ball's position with its surface right at the cushion (to offset it going into the cushion)
		if (pos.z < 0) // colliding with top wall			
			m_balls[nBall]->setPosition(pos.x, pos.y, point_tw.z + rad);
		else		   // colliding with bottom wall
			m_balls[nBall]->setPosition(pos.x, pos.y, point_bw.z - rad);
		// adjust ball's angle and velocity
		ang  = 360 - ang;								// incident angle		
		vel -= WALL_FRICTION;							// reduce ball's velocity
		
		m_balls[nBall]->setAngle(ang);
		m_balls[nBall]->setVelocity(vel);
	}	
}


bool World::executeBallPockCollision(int nBall)
{
	GLfloat vel = m_balls[nBall]->getVelocity();
	if (vel <= 0)
		return false;	
	vec3    pos = m_balls[nBall]->getPosition();
	GLfloat rad = m_balls[nBall]->getRadius();		
	
	//** LEFT BOTTOM POCKET **//
	 // left bottom pocket
	vec3			  normal_lb = pocket_lb.getNormal();
	GLfloat			    dist_lb = dot(pos, normal_lb) - pocket_lb.get_d();
	vec3	 pocketLeftBound_lb = pocket_lb.getInnerLeftBound();
	vec3	pocketRightBound_lb = pocket_lb.getInnerRightBound();
	bool     bInPocketBounds_lb = false;
	if (pos.x + 0.5 * rad <= pocketLeftBound_lb.x && 
		pos.z - 0.5 * rad >= pocketRightBound_lb.z)
	{
		bInPocketBounds_lb = true;
	}

	//** LEFT MIDDLE POCKET **//
	
	vec3			  normal_lm = pocket_lm.getNormal();
	GLfloat			    dist_lm = dot(pos, normal_lm) - pocket_lm.get_d();
	vec3   	 pocketLeftBound_lm = pocket_lm.getInnerLeftBound();
	vec3    pocketRightBound_lm = pocket_lm.getInnerRightBound();	
	bool     bInPocketBounds_lm = false;
	if (pos.z + rad <= pocketLeftBound_lm.z && 
		pos.z - rad >= pocketRightBound_lm.z)
	{
		bInPocketBounds_lm = true;
	}

	//** LEFT TOP POCKET **//
	
	vec3			  normal_lt = pocket_lt.getNormal();
	GLfloat			    dist_lt = dot(pos, normal_lt) - pocket_lt.get_d();
	vec3	 pocketLeftBound_lt = pocket_lt.getInnerLeftBound();
	vec3	pocketRightBound_lt = pocket_lt.getInnerRightBound();
	bool     bInPocketBounds_lt = false;
	if (pos.z + 0.5 * rad >= pocketLeftBound_lt.z && 
		pos.x + 0.5 * rad <= pocketRightBound_lt.x)
	{
		bInPocketBounds_lt = true;
	}

	//**RIGHT BOTTOM POCKET **//
	
	vec3			  normal_rb = pocket_rb.getNormal();
	GLfloat			    dist_rb = dot(pos, normal_rb) - pocket_rb.get_d();
	vec3	 pocketLeftBound_rb = pocket_rb.getInnerLeftBound();
	vec3	pocketRightBound_rb = pocket_rb.getInnerRightBound();
	bool     bInPocketBounds_rb = false;
	if (pos.z - 0.5 * rad >= pocketLeftBound_rb.z && 
		pos.x - 0.5 * rad >= pocketRightBound_rb.x)
	{
		bInPocketBounds_rb = true;
	}

	//** RIGHT MIDDLE POCKET **//
	
	vec3			  normal_rm = pocket_rm.getNormal();
	GLfloat			    dist_rm = dot(pos, normal_rm) - pocket_rm.get_d();
	vec3	 pocketLeftBound_rm = pocket_rm.getInnerLeftBound();
	vec3    pocketRightBound_rm = pocket_rm.getInnerRightBound();
	bool     bInPocketBounds_rm = false;
	if (pos.z - rad >= pocketLeftBound_rm.z && 
		pos.z + rad <= pocketRightBound_rm.z)
	{
		bInPocketBounds_rm = true;
	}

	//**RIGHT TOP POCKET **//
	
	vec3			  normal_rt = pocket_rt.getNormal();
	GLfloat			    dist_rt = dot(pos, normal_rt) - pocket_rt.get_d();
	vec3	 pocketLeftBound_rt = pocket_rt.getInnerLeftBound();
	vec3	pocketRightBound_rt = pocket_rt.getInnerRightBound();
	bool     bInPocketBounds_rt = false;
	if (pos.x - 0.5 * rad >= pocketLeftBound_rt.x && 
		pos.z + 0.5 * rad <= pocketRightBound_rt.z)
	{
		bInPocketBounds_rt = true;
	}

		
	if	(	(bInPocketBounds_lb &&	(abs(dist_lb) <= rad)) ||																				
			(bInPocketBounds_lm &&	(abs(dist_lm) <= rad)) || 
			(bInPocketBounds_lt &&  (abs(dist_lt) <= rad)) ||
			(bInPocketBounds_rb &&  (abs(dist_rb) <= rad)) || 
			(bInPocketBounds_rm &&  (abs(dist_rm) <= rad)) ||
			(bInPocketBounds_rt &&  (abs(dist_rt) <= rad))  
		)
	{	
		m_balls[nBall]->setVelocity(0);		
		m_balls[nBall]->setPocket(true);
		//std::cout << "Ball in pocket!" << std::endl;
		return true;
	}
	return false;
}



void World::updatePosition(GLfloat timediff)  {
	for (int i = 0; i < m_numBalls; i++) {
		vec3 p = m_balls[i]->getPosition();
		GLfloat v = m_balls[i]->getVelocity();
		GLfloat a = m_balls[i]->getAngle();
				
		
		m_balls[i]->setPosition(p.x + v * cos(a * DegreesToRadians), p.y, p.z + v*sin(a * DegreesToRadians));
		m_balls[i]->setVelocity(v - TABLE_FRICTION);
		if (m_balls[i]->getVelocity() < 0.0f) 
			m_balls[i]->setVelocity(0.0f);
		else
		{
			GLfloat v = m_balls[i]->getVelocity();
			GLfloat a = m_balls[i]->getAngle();
			vec3 vel = vec3(v * cos(a * DegreesToRadians), 0, v * sin(a * DegreesToRadians));
			vec3 axis = cross(vec3(0, 1, 0), vel); // ball's axis of rotation
			m_balls[i]->rotate(axis, 100 * m_balls[i]->getVelocity());

		}
		

		

		
		
	}
}

void World::checkBallToBallCollision() {

	vector<collision> c;
	int counter = 0;
	for (int i=0; i< m_numBalls - 1; i++) {
		if (!m_balls[i]->inPocket()) {
			for (int j = i + 1; j < m_numBalls; j++) {
				Ball *b1 = m_balls[i];
				Ball *b2 = m_balls[j];
				if (!b2->inPocket()) {
					GLfloat distance = getDistance(b1, b2);
					if (distance < 4.0){
						//cout << "Ball: " << b1->getType() << "Ball: " << b2->getType() << endl;
						//cout << b1->getPosition() << b2->getPosition() << endl;
						//cout << "Collision Detected!" << endl;
						//c.push_back(collision(b1, b2, distance));
						doCollision(b1, b2);
						//system("pause");
					}
				}
			}
		}
	}
	
	/*orderCollisions(c);
	if (c.size() > 0)
		//cout << "Num Collisions: " << c.size() << endl;
	for (int i = 0; i < c.size(); i++) {
		//cout << "Ball: " << c[i].b1->getType() << "Ball: " << c[i].b2->getType() << endl;
		doCollision(c[i].b1, c[i].b2);
		//cout << c[i].b1->getPosition() << c[i].b2->getPosition() << endl;
		//system("pause");
	}*/
	

	
}

void World::orderCollisions( vector<collision> &c)
{
     int i, j,  numLength = c.size();
	 collision key;
     for(j = 1; j < numLength; j++)    // Start with 1 (not 0)
    {
           key = c[j];
           for(i = j - 1; (i >= 0) && (c[i].d < key.d); i--)   // Smaller values move up
          {
                 c[i+1] = c[i];
          }
         c[i+1] = key;    //Put key into its proper location
     }
     return;
}

void World::doCollision(Ball* b1, Ball* b2) {

	vec3 v1 = vec3(b1->getVelocity() * cos(b1->getAngle() * DegreesToRadians)
				, 0.0, b1->getVelocity() * sin(b1->getAngle() * DegreesToRadians));
	vec3 v2 = vec3(b2->getVelocity() * cos(b2->getAngle() * DegreesToRadians)
				, 0.0, b2->getVelocity() * sin(b2->getAngle() * DegreesToRadians));
	//cout << "Ball " << b1->getType() << "Ball " << b2->getType() << endl;
	//cout << "Start Velocity:  " << v1  << v2 << endl;

	separateBalls(b1, b2);
	
	vec3 normal = b1->getPosition() - b2->getPosition();
	normal = normalize(normal);

	vec3 U1N = (v1.x * normal.x + v1.z * normal.z) * normal;
	vec3 U2N = (v2.x * normal.x + v2.z * normal.z) * normal;

	v1 = v1 - U1N + U2N;
	v2 = v2 - U2N + U1N;


	//cout << "End Velocity:  " << v1  << v2 << endl;

	b1->setVelocity(sqrt(pow(v1.x, 2) + pow(v1.z, 2)));
	b2->setVelocity(sqrt(pow(v2.x, 2) + pow(v2.z, 2)));

	if (v1.x > 0.0f) 
		b1->setAngle(atan(v1.z/v1.x) / DegreesToRadians);
	else if (v1.x < 0.0f)
		b1->setAngle(atan(v1.z/v1.x) / DegreesToRadians + 180.0f);
	else if (v1.x == 0.0 && v1.z >= 0.0f)
		b1->setAngle(90.0f);
	else
		b1->setAngle(270.0f);

	if (v2.x > 0.0f)
		b2->setAngle(atan(v2.z/v2.x) / DegreesToRadians);
	else if (v2.x < 0.0f)
		b2->setAngle(atan(v2.z/v2.x) / DegreesToRadians + 180.0f);
	else if (v2.x == 0.0 && v2.z >= 0.0f)
		b2->setAngle(90.0f);
	else
		b2->setAngle(270.0f);
	
	//cout << "New Velocity: " << b1->getVelocity() << "  Angle: " << b1->getAngle() << endl;
	//cout << "New Velocity: " << b2->getVelocity() << "  Angle: " << b2->getAngle() << endl;

}

GLfloat World::getDistance(Ball* b1, Ball*b2) {
	vec3 p1 = b1->getPosition();
	vec3 p2 = b2->getPosition();
	GLfloat dx = (p2.x - p1.x);
	GLfloat dz = (p2.z - p1.z);
	return pow(dx, 2) + pow(dz, 2);
}
void World::separateBalls(Ball* b1, Ball *b2) {
	GLfloat v1 = b1->getVelocity();
	GLfloat v2 = b2->getVelocity();
	GLfloat a1 = b1->getAngle() + 180.0f;
	GLfloat a2 = b2->getAngle() + 180.0f;
	vec3 p1 = b1->getPosition();
	vec3 p2 = b2->getPosition();
	GLfloat d = getDistance(b1, b2);
	GLfloat d2 = 0.0;
	int count = 0;
	if (v1 > v2) {
		while (d < 4.0) {
			if (d2 > d) {
				a1 += 180.0f;
				//cout << "Came here" << endl;
			}
			count++;
			vec3 p1 = b1->getPosition();
			b1->setPosition(p1.x + v1 * cos(a1 * DegreesToRadians)/10.0, p1.y, p1.z + v1*sin(a1 * DegreesToRadians)/10.0);
			if (count >100) {
				//cout << b1->getType() << " " << b2->getType() << " " ;
				//cout << "I failed1: " << getDistance(b1, b2) << endl;
				//cout << b1->getVelocity() << " " << b2->getVelocity() << endl;
				//system("pause");
				//cout << "Meh..." << endl;
				break;
			}
			d2 = d;
			d = getDistance(b1, b2);
		}
	} else {
		while (d < 4.0) {
			if (d2 > d) {
				a2 += 180.0;
				//cout << "yo mama" << endl;
			}
			vec3 p2 = b2->getPosition();
			count++;
			b2->setPosition(p2.x + v2 * cos(a2 * DegreesToRadians)/10.0, p2.y, p2.z + v2*sin(a2 * DegreesToRadians)/10.0);
			if (count >100) {
				//cout << b1->getType() << " " << b2->getType() << " " ;
				//cout << "I failed2: " << getDistance(b1, b2) << endl;
				//cout << b1->getVelocity() << " " << b2->getVelocity() << endl;
				//system("pause");
				//cout << " Meh!" << endl;
				break;
			}
			d2 = d;
			d = getDistance(b1, b2);
		}
		
	}
}