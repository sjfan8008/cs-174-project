#include "Camera.h"


Camera3D::Camera3D(const vec3& center, GLfloat roll, GLfloat yaw, GLfloat pitch, 
			 GLfloat nearClip, GLfloat farClip, GLfloat fovy, GLfloat aspect)
			 :m_center(center), m_rotation(vec3(0,0,1), roll)
{
	//camera is located at (m_rho, theta, phi) is spherical coordinates.
	//to get the direction the camera is facing we rotate by -theta around
	//the y-axis, and -phi around the x-axis.  the negative z-axis is taken
	//to be theta=0.  The positive x-z plane is taken to be phi=0.
	m_rotation = Quaternion(vec3(0,1,0), yaw) * Quaternion(vec3(1,0,0), pitch) * m_rotation;
	setPerspective(nearClip, farClip, fovy, aspect);
}

//----------------------------------------------------------------------------

void Camera3D::useShaderMatrix(GLuint pMwMatrix)
{
	printf("deprecated");
	exit(1);
	s_worldCameraProjection = pMwMatrix;
}

//----------------------------------------------------------------------------

vec3 Camera3D::getPosition() const
{
	return m_center; 
}

//----------------------------------------------------------------------------

void Camera3D::setPerspective(GLfloat nearClip, GLfloat farClip, GLfloat fovy, GLfloat aspect)
{
	m_perspective = Perspective(fovy, aspect, nearClip, farClip);
}

//----------------------------------------------------------------------------

void Camera3D::faceDirection(const vec3& direction) {

	m_rotation = Quaternion(direction);
}

//----------------------------------------------------------------------------

vec3 Camera3D::getDirection() const {

	return normalize(m_rotation * vec3(0,0,-1));
}

//----------------------------------------------------------------------------

void Camera3D::setCenter(const vec3 &center)
{
	m_center = center;
}

//----------------------------------------------------------------------------

void Camera3D::reset(const vec3& center, GLfloat roll, GLfloat yaw, GLfloat pitch, 
			 GLfloat nearClip, GLfloat farClip, GLfloat fovy, GLfloat aspect)
{
	m_center = center;
	m_rotation = Quaternion(vec3(0,1,0), yaw) * Quaternion(vec3(1,0,0), pitch) * Quaternion(vec3(0,0,1), roll);
	setPerspective(nearClip, farClip, fovy, aspect);
}

//----------------------------------------------------------------------------

void Camera3D::rotateYaw(GLfloat degrees)
{
	m_rotation = Quaternion(vec3(0,1,0), degrees) * m_rotation;
}

//----------------------------------------------------------------------------

void Camera3D::rotatePitch(GLfloat degrees)
{
	vec3 horizontal = m_rotation * vec3(1,0,0);
	m_rotation = Quaternion(horizontal, degrees) * m_rotation;
}

//----------------------------------------------------------------------------

void Camera3D::setYaw(GLfloat degrees) {

	GLfloat currentPitch = getPitch();
	m_rotation = Quaternion(vec3(0,1,0), degrees) * Quaternion(vec3(1,0,0), currentPitch);
}

//----------------------------------------------------------------------------

void Camera3D::setPitch(GLfloat degrees) {
	
	GLfloat yaw = getYaw();
	m_rotation = Quaternion(vec3(0,1,0), yaw) * Quaternion(vec3(1,0,0), degrees);
}

//----------------------------------------------------------------------------

GLfloat Camera3D::getPitch() const {

	return 180/M_PI * asin( getDirection().y );
}

//----------------------------------------------------------------------------

GLfloat Camera3D::getYaw() const {

	vec3 dir = getDirection();
	return 180/M_PI * atan2(-dir.x, -dir.z);
}

//----------------------------------------------------------------------------
/*
void Camera3D::move( GLfloat sideways, GLfloat forward )
{
	vec3 forwardDir = m_rotation * vec3(0,0,-1);
	forwardDir = normalize(forwardDir);

	vec3 sidewaysDir = m_rotation * vec3(1,0,0);
	sidewaysDir.y = 0;							//project sidewaysDir onto the x-z plane
	sidewaysDir = normalize(sidewaysDir);

	m_center += forward*forwardDir + sideways*sidewaysDir;	
}*/

void Camera3D::moveForward(GLfloat s)
{
	vec3 forwardDir = m_rotation * vec3(0,0,-1);
	forwardDir = normalize(forwardDir);
	m_center += s*forwardDir;
}

void Camera3D::changeAltitude(GLfloat y)
{
	m_center += y*vec3(0,1,0);
}

void Camera3D::moveLateral(GLfloat s)
{
	
	vec3 direction = m_rotation * vec3(1,0,0);
	direction.y = 0;							//project onto the x-z plane
	direction = normalize(direction);
	m_center += s*direction;
}

//----------------------------------------------------------------------------

void Camera3D::useView() const
{
	printf("deprecated");
	exit(1);
	mat4 m = m_perspective * this->worldCameraMatrix();
	glUniformMatrix4fv(s_worldCameraProjection, 1, GL_TRUE, m);
}

//----------------------------------------------------------------------------

mat4 Camera3D::worldCameraMatrix() const
{	
	//get the negative conjugate of q.  This is the multiplicative inverse of a quaternion.
	//q now orients the world in camera coordinates, where as m_rotation orients the camera
	//in world coordinates.
	
	Quaternion q = m_rotation.getConjugate();
	q.negate();

	return q.getMatrix() * Translate( -m_center );
}

//----------------------------------------------------------------------------

mat4 Camera3D::getMatrix() const
{
	return worldCameraMatrix();
}

mat4 Camera3D::getPerspective() const
{
	return m_perspective;
}