#include "RigidBody.h"
#include "World.h"
#include <cassert>
using namespace std;

int iPow(int x, int n) {
	if (n == 0)
		return 1;
	int ret = 1;
	for (int i = 0; i < n; i++)
		ret *= x;
	return ret;
}

vec3 v4slice(const point4& v) {
	return vec3(v.x, v.y, v.z);
}

/*****************************************************************/
//							Ball
/*****************************************************************/

int Ball::bufferDataIndex = BUF_DATA_UNINIT;

BufferData Ball::createBallBufferData() {
	
	BufferData mdata;
	int NumTimesToSubdivide = 5;
	bool AverageNormals = true;

	mdata.nVertices = 3 * iPow(4, NumTimesToSubdivide + 1);
	mdata.normals = new vec3[mdata.nVertices];
	mdata.points = new point4[mdata.nVertices];
	mdata.textCoords = new vec2[mdata.nVertices];

	int Index = 0;
	tetrahedron(NumTimesToSubdivide, Index, mdata, AverageNormals);

	return mdata;
}

void
Ball::triangle( const point4& a, const point4& b, const point4& c, int &Index, BufferData& mdata, bool AverageNormals)
{
    vec4 nnormal = cross(b - a, c - b);
	vec3 normal = vec3(nnormal.x, nnormal.y, nnormal.z);

	point4* points = mdata.points;
	vec3* normals = mdata.normals;
	vec2* textCoords = mdata.textCoords;

	const GLfloat TWOPI = 2*M_PI;

	//map (s,t) to (theta, phi)
	GLfloat sA = atan2(a.x, a.z) / (TWOPI) + 0.5;
	GLfloat sB = atan2(b.x, b.z) / (TWOPI) + 0.5;
	GLfloat sC = atan2(c.x, c.z) / (TWOPI) + 0.5;

	//t coordinates start from the bottom of the texture,
	//but (asin(*.y) / M_PI + .5) will be 0 for points at the top  of the
	//sphere, and 1 at the bottom of the sphere.  Subtract this value from 1.0
	//to flip the texture right-side up.
	GLfloat tA = 1.0 - (asin(a.y) / M_PI + .5);
	GLfloat tB = 1.0 - (asin(b.y) / M_PI + .5);
	GLfloat tC = 1.0 - (asin(c.y) / M_PI + .5);

	//Some triangles may end up with one vertex mapped to
	//a point near the left edge of the texture, and another
	//vertex mapped to the right edge of the texture.  The following
	//code fixes such vertices.
	if (sA < .25 && sB > .75) {
		sB = 0;
	}
	else if (sA > .75 && sB < .25) {
		sB = 1.0;
	}

	if (sA < .25 && sC > .75) {
		sC = 0;
	}
	else if (sA > .75 && sC < .25) {
		sC = 1.0;
	}

	vec2 texCoordA(sA,tA);
	vec2 texCoordB(sB,tB);
	vec2 texCoordC(sC,tC);

	if (AverageNormals) {
		vec4 na = normalize(a);
		vec4 nb = normalize(b);
		vec4 nc = normalize(c);

		normals[Index] = vec3(na.x, na.y, na.z); points[Index] = a; textCoords[Index] = texCoordA; Index++;
		normals[Index] = vec3(nb.x, nb.y, nb.z); points[Index] = b; textCoords[Index] = texCoordB; Index++;
		normals[Index] = vec3(nc.x, nc.y, nc.z); points[Index] = c; textCoords[Index] = texCoordC; Index++;
	}
	else {
		normals[Index] = normal;  points[Index] = a; textCoords[Index] = texCoordA;  Index++;
		normals[Index] = normal;  points[Index] = b; textCoords[Index] = texCoordB; Index++;
		normals[Index] = normal;  points[Index] = c; textCoords[Index] = texCoordC; Index++;
	}
}

point4
Ball::unit( const point4& p )
{
    float len = p.x*p.x + p.y*p.y + p.z*p.z;
    
    point4 t;
    if ( len > DivideByZeroTolerance ) {
	t = p / sqrt(len);
	t.w = 1.0;
    }

    return t;
}

void
Ball::divide_triangle( const point4& a, const point4& b,
		 const point4& c, int count, int& Index, BufferData& mdata, bool AverageNormals )
{
    if ( count > 0 ) {
        point4 v1 = unit( a + b );
        point4 v2 = unit( a + c );
        point4 v3 = unit( b + c );
        divide_triangle(  a, v1, v2, count - 1, Index, mdata, AverageNormals );
        divide_triangle(  c, v2, v3, count - 1, Index, mdata, AverageNormals );
        divide_triangle(  b, v3, v1, count - 1, Index, mdata, AverageNormals );
        divide_triangle( v1, v3, v2, count - 1, Index, mdata, AverageNormals );
    }
    else {
        triangle( a, b, c, Index, mdata, AverageNormals );
    }
}

void
Ball::tetrahedron( int count, int& Index, BufferData& mdata, bool AverageNormals )
{
	int start = Index;

    point4 v[4] = {
	vec4( 0.0, 0.0, 1.0, 1.0 ),
	vec4( 0.0, 0.942809, -0.333333, 1.0 ),
	vec4( -0.816497, -0.471405, -0.333333, 1.0 ),
	vec4( 0.816497, -0.471405, -0.333333, 1.0 )
    };

    divide_triangle( v[0], v[1], v[2], count, Index, mdata, AverageNormals );
    divide_triangle( v[3], v[2], v[1], count, Index, mdata, AverageNormals );
    divide_triangle( v[0], v[3], v[1], count, Index, mdata, AverageNormals );
    divide_triangle( v[0], v[2], v[3], count, Index, mdata, AverageNormals );
}

//----------------------------------------------------------------------------

Ball::Ball() {

}

Ball::Ball(vec3 pos, GLfloat vel, GLfloat mass, GLfloat angle, GLfloat radius, int type,
			BufferManager& bufMan, const Texture& texture, const Material& material, GLuint shadingType)
{
	//physical properties and position
	m_velocity = vel;
	m_mass = mass;
	m_angle = angle;
	m_radius = radius;
	m_type = type;
	m_pocket = false;
	//setPosition(pos.x, pos.y, pos.z);
	scale(radius);

	//graphics initialization
	if ( ! bufMan.isLoaded(Ball::bufferDataIndex) )
	{
		//Ball geometry has not been loaded yet.  Load it
		//and save the index.
		BufferData newBufferData = Ball::createBallBufferData();
		Ball::bufferDataIndex = bufMan.loadBufferData(newBufferData);
	}

	//pass model data to the base class
	ModelData md;
	md.shadingType = shadingType;
	md.texture = texture.getGLtextObject();
	md.material = material;
	md.bufMan = &bufMan;
	Model::Init(md);
	setPosition(pos.x, pos.y, pos.z);

}

Ball::Ball(vec3 pos, GLfloat vel, GLfloat mass, GLfloat angle, GLfloat radius, int type) {
	m_velocity = vel;
	m_mass = mass;
	m_angle = angle;
	m_radius = radius;
	m_type = type;
	m_pocket = false;
	setPosition(pos.x, pos.y, pos.z);
}

Ball::Ball(const Ball& other)
:Model(other)
{
}

Ball::~Ball() {

}


Ball& Ball::operator=(const Ball& other) {

	return *this;
}

int Ball::getBufferDataIndex() const {
	return Ball::bufferDataIndex;
}

int Ball::getType() const {
	return m_type;
}

GLfloat Ball::getMass() const {

	return m_mass;
}

GLfloat Ball::getVelocity() const {

	return m_velocity;
}

void Ball::setVelocity(GLfloat v) {
	m_velocity = v;
}

GLfloat Ball::getAngle() const {
	return m_angle;
}

void Ball::setAngle(GLfloat a) {
	m_angle = a;
}

bool Ball::inPocket() const {
	return m_pocket;
}

void Ball::setPocket(bool b) {
	m_pocket = b;
}

void Ball::draw() const {
	if (!m_pocket)
		Model::draw();
}

/*****************************************************************/
//						Tetrahedron			
/*****************************************************************/

void quad( int a, int b, int c, int d, int& Index, BufferData* mdata, const point4 vertices[])
{
	point4* points = mdata->points;
	vec3* normals = mdata->normals;
	vec2* textCoords = mdata->textCoords;

    // Initialize temporary vectors along the quad's edge to
    //   compute its face normal 
    vec4 u = vertices[b] - vertices[a];
    vec4 v = vertices[c] - vertices[b];

    vec3 normal = normalize( cross(vec3(u.x,u.y,u.z), vec3(v.x,v.y,v.z)) );

	//vertices[a] == upper left corner of the quad
	//vertices[b] == bottom left corner of the quad
	//vertices[c] == bottom right corner of the quad
	//vertices[d] == top right corner of the quad

    normals[Index] = normal; points[Index] = vertices[a]; textCoords[Index] = vec2(0,0); Index++;
    normals[Index] = normal; points[Index] = vertices[b]; textCoords[Index] = vec2(0,1); Index++;
    normals[Index] = normal; points[Index] = vertices[c]; textCoords[Index] = vec2(1,1); Index++;
    normals[Index] = normal; points[Index] = vertices[a]; textCoords[Index] = vec2(0,0); Index++;
    normals[Index] = normal; points[Index] = vertices[c]; textCoords[Index] = vec2(1,1); Index++;
    normals[Index] = normal; points[Index] = vertices[d]; textCoords[Index] = vec2(1,0); Index++;
}

void Tetrahedron::genVertices(BufferData* mdata, const point4 vertices[8]) 
{
	if (!mdata)
		return;

	//initialze the position of all cube verticies
	int nVertices = 36;
	mdata->nVertices = nVertices;
	mdata->points = new point4[nVertices];
	mdata->normals = new vec3[nVertices];
	mdata->textCoords = new vec2[nVertices];

	int index = 0;
	quad( 1, 0, 3, 2, index, mdata, vertices);
    quad( 2, 3, 7, 6, index, mdata, vertices );
    quad( 3, 0, 4, 7, index, mdata, vertices );
    quad( 6, 5, 1, 2, index, mdata, vertices );
    quad( 4, 5, 6, 7, index, mdata, vertices );
    quad( 5, 4, 0, 1, index, mdata, vertices );
}

Tetrahedron::Tetrahedron(BufferManager& bufMan, const Texture& text, const Material& material, const point4 vertices[8])
{
	setPosition(0,0,0);
	scale(1.0);

	BufferData mdata;
	Tetrahedron::genVertices(&mdata, vertices);
	m_bufferIndex = bufMan.loadBufferData(mdata);

	//pass model data to the base class
	ModelData md;
	md.shadingType = GOURAUD;
	md.texture = text.getGLtextObject();
	md.material = material;
	md.bufMan = &bufMan;
	Model::Init(md);

	//		With Zero Rotation:

	/*				Points	
					    
			   5-----------------6
			  /|			   /|
			 / |			  /	|
			/  |			 /	|
		   1----------------2   |
			|  |			| 	|   
			|  |			|	|   
			|  |4-------------7/
			| /				| /
			|/				|/
			0---------------3
					
	*/


	/*				Faces	
					    4
			   ------------------
			  /				   / |
			 /		0		  /	 |
			/				 /	 |
			-----------------  3 |
		1	|				|    |
			|				|   /
			|		2		|  /
			|				| /
			|				|/
			-----------------
					5
	*/
}

Tetrahedron::~Tetrahedron() {

}

Tetrahedron::Tetrahedron(const Tetrahedron& other)
	: Model(other)
{
}

Tetrahedron& Tetrahedron::operator=(const Tetrahedron& other) {

	return *this;
}

const BoundingRectangle* Tetrahedron::getFace(int i) const {
	return m_p[i];
}

void Tetrahedron::draw() const{
	Model::draw();
}

/*****************************************************************/
//							Stick
/*****************************************************************/

int Stick::m_bufferIndex = BUF_DATA_UNINIT;

Stick::Stick(BufferManager& bufMan, const Texture& text, const Material& material, GLfloat frontRadius, GLfloat backRadius, GLfloat length) 
{
	setPosition(0,0,0);
	scale(1.0);

	if ( ! bufMan.isLoaded(m_bufferIndex) )
	{
		BufferData mdata;
		Stick::genVertices(&mdata, frontRadius, backRadius, length, 30);
		m_bufferIndex = bufMan.loadBufferData(mdata);
	}

	ModelData md;
	md.shadingType = PHONG;
	md.texture = text.getGLtextObject();
	md.material = material;
	md.bufMan = &bufMan;
	Model::Init(md);
}

void Stick::genVertices(BufferData* bdata, GLfloat frontRadius, GLfloat backRadius, GLfloat length, int numQuads) {

	assert(3 <= numQuads);

	//generate front circle.  It will have numQuads distinct vertices, but the last element
	//is a copy of the first
	point4* frontCircle = new point4[numQuads+1];
	createCircle(frontCircle, frontRadius, 0, numQuads);

	//generate back circle
	point4* backCircle = new point4[numQuads+1];
	createCircle(backCircle, backRadius, length, numQuads);

	//6*numQuads vertices for the side of the cylinder, and 3*numQuads for the top and bottom surfaces
	int nVertices = 6*numQuads + 3*numQuads + 3*numQuads;
	bdata->nVertices = nVertices;
	bdata->points = new point4[nVertices];
	bdata->normals = new vec3[nVertices];
	bdata->textCoords = new vec2[nVertices];

	int Index = 0;
	//create vertices that connect the front and back circles
	createSideVertices(bdata, Index, frontCircle, backCircle, numQuads);

	//create vertices to cover the surface front circle
	createEndVertices(bdata, Index, frontCircle, 0, numQuads);

	//create vertices to cover the surface of the back circle
	createEndVertices(bdata, Index, backCircle, length, numQuads); 

	delete[] frontCircle;
	delete[] backCircle;
}

void Stick::createCircle(point4 buffer[], GLfloat radius, GLfloat zcoord, int numPoints) {

	//Assumption: buffer has room for numPoints + 1 elements

	GLfloat d_theta = TWO_PI / numPoints;
	GLfloat angle = 0;

	for (int i = 0; i < numPoints; i++) {

		GLfloat xcoord = radius*cos(angle);
		GLfloat ycoord = radius*sin(angle);
		buffer[i] = point4(xcoord, ycoord, zcoord, 1.0);
		angle += d_theta;
	}

	//copy the first element to the end.
	buffer[numPoints] = buffer[0];
}

void Stick::createSideVertices(BufferData* bdata, int& Index, point4 frontCircle[], point4 backCircle[], int numQuads) {

	//Assumptions: frontCircle and backCircle both have numQuads+1 elements
	//			   bdata has room for at least Index + numQuads vertices

	for (int i = 0; i < numQuads; i++) {

		//triangle 1
		sideVertex(bdata, Index, frontCircle[i]);
		sideVertex(bdata, Index, backCircle[i]);
		sideVertex(bdata, Index, backCircle[i+1]);

		//triangle 2
		sideVertex(bdata, Index, frontCircle[i]);
		sideVertex(bdata, Index, backCircle[i+1]);
		sideVertex(bdata, Index, frontCircle[i+1]);
	}
}

void Stick::createEndVertices(BufferData* bdata, int& Index, point4 circlePoints[], int zcoord, int numCirclePoints) {

	point4 center = point4(0, 0, zcoord, 1.0);

	for (int i = 0; i < numCirclePoints; i++) {

		vec3 normal = normalize( v4slice(cross( circlePoints[i] - center, circlePoints[i+1] - center )) );

		endVertex(bdata, Index, center, normal);
		endVertex(bdata, Index, circlePoints[i], normal);
		endVertex(bdata, Index, circlePoints[i+1], normal);
	}
}

void Stick::sideVertex(BufferData* bdata, int& Index, const point4& p) {

	point4* points = bdata->points;
	vec3* normals = bdata->normals;
	vec2* textCoords = bdata->textCoords;

	//Assumption: p.y*p.y + p.x*p.x > 0
	GLfloat s = (asin( p.y / (sqrt(p.y*p.y + p.x*p.x))) / M_PI) + 0.5;
	GLfloat t = p.z > 0 ? 0 : 1.0;

	points[Index] = p;
	normals[Index] = normalize( vec3(p.x, p.y, 0) );
	textCoords[Index] = vec2(t,s); //TODO: calculate text coord
	Index++;
}

void Stick::endVertex(BufferData* bdata, int& Index, const point4& p, const vec3& normal) {

	point4* points = bdata->points;
	vec3* normals = bdata->normals;
	vec2* textCoords = bdata->textCoords;

	GLfloat s, t;
	if (p.x*p.x + p.y*p.y == 0) {
		s = 0;
		t = 0;
	}
	else {
		s = (asin( p.y / (sqrt(p.y*p.y + p.x*p.x))) / M_PI) + 0.5;
		t = 1.0; 
	}

	points[Index] = p;
	normals[Index] = normal;
	textCoords[Index] = vec2(s,t);
	Index++;
}

/*****************************************************************/
//							Table
/*****************************************************************/

/***********************    Geometry    ****************************/

void genBoxVertices(point4 vertices[8], GLfloat xlen, GLfloat ylen, GLfloat zlen) {
	
	vertices[0] = point4( -xlen/2, -ylen/2,  zlen/2, 1.0 );
	vertices[1] = point4( -xlen/2,  ylen/2,  zlen/2, 1.0 );
	vertices[2] = point4(  xlen/2,  ylen/2,  zlen/2, 1.0 );
	vertices[3] = point4(  xlen/2, -ylen/2,  zlen/2, 1.0 );
	vertices[4] = point4( -xlen/2, -ylen/2, -zlen/2, 1.0 );
	vertices[5] = point4( -xlen/2,  ylen/2, -zlen/2, 1.0 );
	vertices[6] = point4(  xlen/2,  ylen/2, -zlen/2, 1.0 );
	vertices[7] = point4(  xlen/2, -ylen/2, -zlen/2, 1.0 );
}

const GLfloat pocketWidth = 2.0;
const GLfloat cornerPocketSideLen = pocketWidth / sin(M_PI/4);

const GLfloat TableWidth = 20;
const GLfloat TableHeight = 35;
const GLfloat SurfaceDepth = 1;

const GLfloat sideBumperWidth = 1.5;
const GLfloat sideBumperHeight = TableHeight;
const GLfloat sideBumperDepth = 1.0;

const GLfloat sideSubBumperWidth = sideBumperWidth;
const GLfloat sideSubBumperHeight = (sideBumperHeight - 2*cornerPocketSideLen - pocketWidth ) / 2;
const GLfloat sideSubBumperDepth = sideBumperDepth;

const GLfloat topBottomBumperWidth = TableWidth;
const GLfloat topBottomBumperHeight = 1.5;
const GLfloat topBottomBumperDepth = 1.0;

const GLfloat topBottomSubBumperWidth = topBottomBumperWidth - 2*cornerPocketSideLen;
const GLfloat topBottomSubBumperHeight = topBottomBumperHeight;
const GLfloat topBottomSubBumperDepth = topBottomBumperDepth;

const GLfloat legWidth = 1.0;
const GLfloat legHeight = 1.0;
const GLfloat legDepth = 15;

Table::Table(BufferManager& bufMan) 
	: Movable(0, 0, 0, 1.0, Quaternion(1.0, 0, 0, 0))
{
	Material woodMaterial = { color4( 0.05f, 0.05f, 0.05f, 1.0f ),
							 color4( 0.5f, 0.5f, 0.5f, 1.0f ),
							 color4( 0.0f, 0.0f, 0.0f, 1.0f ),
							 10.0f};

	Texture woodTexture("Textures/wood.jpg");
	Texture feltTexture("Textures/felt.jpg");

	//Instantiate all components of the table.  The point returned by Table::getPosition()
	//shall be a point on the table's surface, in the exact center (eg. halfway between the
	//left and right bumpers, and halfway between the top and bottom bumpers)

	point4 surfaceVertices[8];
	point4 sideBumperVertices[8];
	point4 sideSubBumperVertices[8];
	point4 topBottomBumperVertices[8];
	point4 topBottomSubBumperVertices[8];
	point4 legVertices[8];

	genBoxVertices(surfaceVertices, TableWidth, SurfaceDepth, TableHeight);
	genBoxVertices(sideBumperVertices, sideBumperWidth, sideBumperDepth, sideBumperHeight);
	genBoxVertices(sideSubBumperVertices, sideSubBumperWidth, sideSubBumperDepth, sideSubBumperHeight);
	genBoxVertices(topBottomBumperVertices, topBottomBumperWidth, topBottomBumperDepth, topBottomBumperHeight);
	genBoxVertices(topBottomSubBumperVertices, topBottomSubBumperWidth, topBottomSubBumperDepth, topBottomSubBumperHeight);
	genBoxVertices(legVertices, legWidth, legDepth, legHeight);

	GLuint index = 0;

	//surface
	m_components[index] = new Tetrahedron(bufMan, feltTexture, woodMaterial, surfaceVertices);
	m_components[index]->setPosition( 0, -SurfaceDepth/2, 0 );
	index++;

	//left cushion
	m_components[index] = new Tetrahedron(bufMan, woodTexture, woodMaterial, sideBumperVertices);
	m_components[index]->setPosition( -TableWidth/2 - sideBumperWidth/2, SurfaceDepth -SurfaceDepth/2, 0 );

	mat4 trans = Translate(sideSubBumperWidth, 0, 0) * m_components[index]->worldTransform();
	m_wallRects[LEFT_CUSION][0] = trans*sideBumperVertices[2]; 
	m_wallRects[LEFT_CUSION][1] = trans*sideBumperVertices[3];
	m_wallRects[LEFT_CUSION][2] = trans*sideBumperVertices[7];
	m_wallRects[LEFT_CUSION][3] = trans*sideBumperVertices[6];

	index++;

	//left sub cushions

		//bottom-left sub cushion
	m_components[index] = new Tetrahedron(bufMan, feltTexture, woodMaterial, sideSubBumperVertices);
	m_components[index]->setPosition(-TableWidth/2 + sideBumperWidth/2,  SurfaceDepth - SurfaceDepth/2, TableHeight/2 - cornerPocketSideLen - sideSubBumperHeight/2);
	
	//store vertices for bounding rects
	trans = m_components[index]->worldTransform();
	m_pocketRects[POCKET_LEFT_BOTTOM][2] = trans * sideSubBumperVertices[3];
	m_pocketRects[POCKET_LEFT_BOTTOM][3] = trans * sideSubBumperVertices[2];
	m_pocketRects[POCKET_LEFT_MIDDLE][0] = trans * sideSubBumperVertices[6];
	m_pocketRects[POCKET_LEFT_MIDDLE][1] = trans * sideSubBumperVertices[7];
	
	index++;

		//top-left sub cushion
	m_components[index] = new Tetrahedron(bufMan, feltTexture, woodMaterial, sideSubBumperVertices);
	m_components[index]->setPosition(-TableWidth/2 + sideBumperWidth/2,  SurfaceDepth - SurfaceDepth/2, -TableHeight/2 + cornerPocketSideLen + sideSubBumperHeight/2);

	//store vertices
	trans = m_components[index]->worldTransform();
	m_pocketRects[POCKET_LEFT_TOP][0] = trans * sideSubBumperVertices[6];
	m_pocketRects[POCKET_LEFT_TOP][1] = trans * sideSubBumperVertices[7];
	m_pocketRects[POCKET_LEFT_MIDDLE][2] = trans * sideSubBumperVertices[3];
	m_pocketRects[POCKET_LEFT_MIDDLE][3] = trans * sideSubBumperVertices[2];

	index++;

	//right cushion
	m_components[index] = new Tetrahedron(bufMan, woodTexture, woodMaterial, sideBumperVertices);
	m_components[index]->setPosition( TableWidth/2 + sideBumperWidth/2, SurfaceDepth -SurfaceDepth/2, 0 );

	trans = Translate(-sideSubBumperWidth, 0, 0) * m_components[index]->worldTransform();
	m_wallRects[RIGHT_CUSION][0] = trans*sideBumperVertices[5];
	m_wallRects[RIGHT_CUSION][1] = trans*sideBumperVertices[4];
	m_wallRects[RIGHT_CUSION][2] = trans*sideBumperVertices[0];
	m_wallRects[RIGHT_CUSION][3] = trans*sideBumperVertices[1];

	index++;

	//right sub cushions

		//bottom-right sub cushion
	m_components[index] = new Tetrahedron(bufMan, feltTexture, woodMaterial, sideSubBumperVertices);
	m_components[index]->setPosition(TableWidth/2 - sideBumperWidth/2,  SurfaceDepth - SurfaceDepth/2, TableHeight/2 - cornerPocketSideLen - sideSubBumperHeight/2);
	
	//store vertices needed for bounding rects
	trans = m_components[index]->worldTransform();
	m_pocketRects[POCKET_RIGHT_BOTTOM][0] = trans * sideSubBumperVertices[1];
	m_pocketRects[POCKET_RIGHT_BOTTOM][1] = trans * sideSubBumperVertices[0];
	m_pocketRects[POCKET_RIGHT_MIDDLE][2] = trans * sideSubBumperVertices[4];
	m_pocketRects[POCKET_RIGHT_MIDDLE][3] = trans * sideSubBumperVertices[5];
	
	index++;

		//top-right sub cushion
	m_components[index] = new Tetrahedron(bufMan, feltTexture, woodMaterial, sideSubBumperVertices);
	m_components[index]->setPosition(TableWidth/2 - sideBumperWidth/2,  SurfaceDepth - SurfaceDepth/2, -TableHeight/2 + cornerPocketSideLen + sideSubBumperHeight/2);
	
	//store vertices needed for bounding rects
	trans = m_components[index]->worldTransform();
	m_pocketRects[POCKET_RIGHT_TOP][2] = trans * sideSubBumperVertices[4];
	m_pocketRects[POCKET_RIGHT_TOP][3] = trans * sideSubBumperVertices[5];
	m_pocketRects[POCKET_RIGHT_MIDDLE][0] = trans * sideSubBumperVertices[1];
	m_pocketRects[POCKET_RIGHT_MIDDLE][1] = trans * sideSubBumperVertices[0];
	
	index++;

	//bottom cusion 
	m_components[index] = new Tetrahedron(bufMan, woodTexture, woodMaterial, topBottomBumperVertices);
	m_components[index]->setPosition( 0, SurfaceDepth - SurfaceDepth/2, TableHeight/2 + topBottomBumperHeight/2 );

	//store vertices needed for bounding rects
	trans = Translate(0, 0, -topBottomSubBumperHeight) * m_components[index]->worldTransform();
	m_wallRects[BOTTOM_CUSION][0] = trans*topBottomBumperVertices[6];
	m_wallRects[BOTTOM_CUSION][1] = trans*topBottomBumperVertices[7];
	m_wallRects[BOTTOM_CUSION][2] = trans*topBottomBumperVertices[4];
	m_wallRects[BOTTOM_CUSION][3] = trans*topBottomBumperVertices[5];

	index++;

	//bottom sub cushion

	m_components[index] = new Tetrahedron(bufMan, feltTexture, woodMaterial, topBottomSubBumperVertices);
	m_components[index]->setPosition(0,  SurfaceDepth - SurfaceDepth/2, TableHeight/2 - topBottomBumperHeight/2);

	//store vertices needed for bounding rects
	trans = m_components[index]->worldTransform();
	m_pocketRects[POCKET_LEFT_BOTTOM][0] = trans * topBottomSubBumperVertices[5];
	m_pocketRects[POCKET_LEFT_BOTTOM][1] = trans * topBottomSubBumperVertices[4];
	m_pocketRects[POCKET_RIGHT_BOTTOM][2] = trans * topBottomSubBumperVertices[7];
	m_pocketRects[POCKET_RIGHT_BOTTOM][3] = trans * topBottomSubBumperVertices[6];

	index++;

	//top cushion

	m_components[index] = new Tetrahedron(bufMan, woodTexture, woodMaterial, topBottomBumperVertices);
	m_components[index]->setPosition( 0, SurfaceDepth - SurfaceDepth/2, -TableHeight/2 - topBottomBumperHeight/2 );

	//1 0 3 2
	trans = Translate(0, 0, topBottomSubBumperHeight) * m_components[index]->worldTransform();
	m_wallRects[TOP_CUSION][0] = trans*topBottomBumperVertices[1];
	m_wallRects[TOP_CUSION][1] = trans*topBottomBumperVertices[0];
	m_wallRects[TOP_CUSION][2] = trans*topBottomBumperVertices[3];
	m_wallRects[TOP_CUSION][3] = trans*topBottomBumperVertices[2];

	index++;

	//top sub-cushion
	m_components[index] = new Tetrahedron(bufMan, feltTexture, woodMaterial, topBottomSubBumperVertices);
	m_components[index]->setPosition(0,  SurfaceDepth - SurfaceDepth/2, -TableHeight/2 + topBottomBumperHeight/2);

	//store vertices for bounding rects
	trans = m_components[index]->worldTransform();
	m_pocketRects[POCKET_LEFT_TOP][2] = trans * topBottomSubBumperVertices[0];
	m_pocketRects[POCKET_LEFT_TOP][3] = trans * topBottomSubBumperVertices[1];
	m_pocketRects[POCKET_RIGHT_TOP][0] = trans * topBottomSubBumperVertices[2];
	m_pocketRects[POCKET_RIGHT_TOP][1] = trans * topBottomSubBumperVertices[3];

	index++;


//legs

	//bottom left
	m_components[index] = new Tetrahedron(bufMan, woodTexture, woodMaterial, legVertices);
	m_components[index]->setPosition( -TableWidth/2 + legWidth/2, -legDepth/2 - SurfaceDepth/2, TableHeight/2 - legHeight/2 );
	index++;

	//bottom right
	m_components[index] = new Tetrahedron(bufMan, woodTexture, woodMaterial, legVertices);
	m_components[index]->setPosition( TableWidth/2 - legWidth/2, -legDepth/2 - SurfaceDepth/2, TableHeight/2 - legHeight/2 );
	index++;

	//top left
	m_components[index] = new Tetrahedron(bufMan, woodTexture, woodMaterial, legVertices);
	m_components[index]->setPosition( -TableWidth/2 + legWidth/2, -legDepth/2 - SurfaceDepth/2, -TableHeight/2 + legHeight/2 );
	index++;

	//top right
	m_components[index] = new Tetrahedron(bufMan, woodTexture, woodMaterial, legVertices);
	m_components[index]->setPosition( TableWidth/2 - legWidth/2, -legDepth/2 - SurfaceDepth/2, -TableHeight/2 + legHeight/2 );
	index++;

	assert(index == m_nComponents);
}

Table::~Table() {

	for (int i = 0; i < m_nComponents; i++) {
		const Tetrahedron* component = m_components[i];
		if (component)
			delete component;
	}
}

/*
Table::Table(const Table& other) {

	Tetrahedron* m_leftCusion = other.m_leftCusion ? new Tetrahedron(*(other.m_leftCusion)) : NULL;
	Tetrahedron* m_rightCusion = other.m_rightCusion ? new Tetrahedron(*(other.m_rightCusion)) : NULL;
	Tetrahedron* m_bottomCusion = other.m_bottomCusion ? new Tetrahedron(*(other.m_bottomCusion)) : NULL;
	Tetrahedron* m_topCusion = other.m_topCusion ? new Tetrahedron(*(other.m_topCusion)) : NULL;
	Tetrahedron* m_surface = other.m_surface ? new Tetrahedron(*(other.m_surface)) : NULL;
	for (int i = 0; i < 4; i++) {
		const Tetrahedron* otherleg = other.m_legs[i];
		m_legs[i] = otherleg ? new Tetrahedron(*otherleg) : NULL;
	}

	int m_nComponents = other.m_nComponents;

	for (int i = 0; i < 6; i++) {
		m_pocketPlanes[6] = Plane(other.m_pocketPlanes[i]);
		m_cusionLines[6] = Line(other.m_cusionLines[i]);
		m_pocketLines[6] = Line(other.m_pocketLines[i]); 
	}
}

	
Table& Table::operator=(const Table& other) {


	return *this;
}
*/

int Table::numComponents() const {
	return m_nComponents;
}

const Tetrahedron* Table::getComponent(int i) const {
	if (0 <= i && i < m_nComponents)
		return m_components[i];
	return NULL;
}

BoundingRectangle Table::getPocketBoundingPlane(Pocket pocket) const {

	mat4 wMm = worldTransform();
	vec3 p1 = v4slice(wMm * m_pocketRects[pocket][0]);
	vec3 p2 = v4slice(wMm * m_pocketRects[pocket][1]);
	vec3 p3 = v4slice(wMm * m_pocketRects[pocket][2]);
	vec3 p4 = v4slice(wMm * m_pocketRects[pocket][3]);

	return BoundingRectangle(p1,p2,p3,p4);
}

BoundingRectangle Table::getCusionPlane(Cusion cusion) const {

	mat4 wMm = worldTransform();
	vec3 p1 = v4slice(wMm * m_wallRects[cusion][0]);
	vec3 p2 = v4slice(wMm * m_wallRects[cusion][1]);
	vec3 p3 = v4slice(wMm * m_wallRects[cusion][2]);
	vec3 p4 = v4slice(wMm * m_wallRects[cusion][3]);

	return BoundingRectangle(p1,p2,p3,p4);
}