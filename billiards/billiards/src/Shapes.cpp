#include "Shapes.h"
#include "Quaternion.h"

/****************************************************************************
*									Sphere
*****************************************************************************/

//----------------------------------------------------------------------------

GLuint Sphere::genVertices(ModelData *mdata, GLuint NumTimesToSubdivide, bool AverageNormals)
{
	if (!mdata)
		return 0;

	mdata->nVertices = 3 * iPow(4, NumTimesToSubdivide + 1);
	mdata->normals = new vec3[mdata->nVertices];
	mdata->points = new point4[mdata->nVertices];
	mdata->textCoords = new vec2[mdata->nVertices];

	int Index = 0;
	tetrahedron(NumTimesToSubdivide, Index, mdata->points, mdata->normals, AverageNormals);

	return Index;
}

//----------------------------------------------------------------------------

void
Sphere::triangle( const point4& a, const point4& b, const point4& c, int &Index, point4 points[], vec3 normals[], bool AverageNormals)
{
    vec4 nnormal = cross(b - a, c - b);
	vec3 normal = vec3(nnormal.x, nnormal.y, nnormal.z);

	//TODO: map texture coordinates to the sphere surface
	vec2 texCoordA(0,0);
	vec2 texCoordB(0,0);
	vec2 texCoordC(0,0);

	if (AverageNormals) {
		vec4 na = normalize(a);
		vec4 nb = normalize(b);
		vec4 nc = normalize(c);

		normals[Index] = vec3(na.x, na.y, na.z); points[Index] = a;  Index++;
		normals[Index] = vec3(nb.x, nb.y, nb.z); points[Index] = b;  Index++;
		normals[Index] = vec3(nc.x, nc.y, nc.z); points[Index] = c;  Index++;
	}
	else {
		normals[Index] = normal;  points[Index] = a;  Index++;
		normals[Index] = normal;  points[Index] = b;  Index++;
		normals[Index] = normal;  points[Index] = c;  Index++;
	}
}

//----------------------------------------------------------------------------

point4
Sphere::unit( const point4& p )
{
    float len = p.x*p.x + p.y*p.y + p.z*p.z;
    
    point4 t;
    if ( len > DivideByZeroTolerance ) {
	t = p / sqrt(len);
	t.w = 1.0;
    }

    return t;
}

void
Sphere::divide_triangle( const point4& a, const point4& b,
		 const point4& c, int count, int& Index, point4 points[], vec3 normals[], bool AverageNormals )
{
    if ( count > 0 ) {
        point4 v1 = unit( a + b );
        point4 v2 = unit( a + c );
        point4 v3 = unit( b + c );
        divide_triangle(  a, v1, v2, count - 1, Index, points, normals, AverageNormals );
        divide_triangle(  c, v2, v3, count - 1, Index, points, normals, AverageNormals );
        divide_triangle(  b, v3, v1, count - 1, Index, points, normals, AverageNormals );
        divide_triangle( v1, v3, v2, count - 1, Index, points, normals, AverageNormals );
    }
    else {
        triangle( a, b, c, Index, points, normals, AverageNormals );
    }
}

void
Sphere::tetrahedron( int count, int& Index, point4 points[], vec3 normals[], bool AverageNormals )
{
	int start = Index;

    point4 v[4] = {
	vec4( 0.0, 0.0, 1.0, 1.0 ),
	vec4( 0.0, 0.942809, -0.333333, 1.0 ),
	vec4( -0.816497, -0.471405, -0.333333, 1.0 ),
	vec4( 0.816497, -0.471405, -0.333333, 1.0 )
    };

    divide_triangle( v[0], v[1], v[2], count, Index, points, normals, AverageNormals );
    divide_triangle( v[3], v[2], v[1], count, Index, points, normals, AverageNormals );
    divide_triangle( v[0], v[3], v[1], count, Index, points, normals, AverageNormals );
    divide_triangle( v[0], v[2], v[3], count, Index, points, normals, AverageNormals );
}

//----------------------------------------------------------------------------

Sphere::Sphere(const ModelData& mdata)
	: Model(mdata), m_qRotation(1.0,0,0,0), 
	m_numVertices(mdata.nVertices)
{
	Model::setPosition(0,0,0);
	Model::scale(1.0);
	m_material.ambient = color4(1,1,1,1);
	m_material.specular = color4(1,1,1,1);
	m_material.diffuse = color4(1,1,1,1);
	m_shadingType = 0;
}

//----------------------------------------------------------------------------

Sphere::Sphere(GLfloat x, GLfloat y, GLfloat z, const ModelData& mdata, const Material& material, GLuint shadingType, GLfloat scale)
	: Model(mdata), m_qRotation(1.0,0,0,0), m_numVertices(mdata.nVertices), m_material(material)
{
	Model::setPosition(x,y,z);
	Model::scale(scale);
	m_shadingType = shadingType;
}

//----------------------------------------------------------------------------

vec3 Sphere::getPosition() const {

	return Model::getPosition();
}

//----------------------------------------------------------------------------

GLuint Sphere::getNumVertices() const
{
	return m_numVertices;
}

//----------------------------------------------------------------------------

Material Sphere::getMaterial() const
{
	return m_material;
}

//----------------------------------------------------------------------------

GLuint Sphere::getShadingType() const
{
	return m_shadingType;
}

//----------------------------------------------------------------------------

void Sphere::setRotation(GLfloat xDegrees, GLfloat yDegrees, GLfloat zDegrees)
{
	m_qRotation = Quaternion(vec3(1,0,0),xDegrees) * Quaternion(vec3(0,1,0), yDegrees) * Quaternion(vec3(0,0,1), zDegrees);
}

//----------------------------------------------------------------------------

void Sphere::incRotation(GLfloat degrees, const vec3 &axis)
{
	Model::rotate(axis, degrees);
	//m_qRotation = Quaternion(axis, degrees) * m_qRotation;
}

//----------------------------------------------------------------------------

void Sphere::setLocation(GLfloat x, GLfloat y, GLfloat z)
{
	Model::setPosition(x,y,z);
}

//----------------------------------------------------------------------------

mat4 Sphere::worldTransform() const
{
	return Model::worldTransform();
}

void Sphere::draw() const
{
	Model::draw();
}


/****************************************************************************
*								Planet
*****************************************************************************/

Planet::Planet(const vec3& orbitPoint, GLfloat radius, GLfloat angularVelocity, GLfloat theta,
		   const ModelData& mdata, const Material& material, GLuint ShadingType, GLfloat scale) 
		   : Sphere(orbitPoint.x, orbitPoint.y, orbitPoint.z, mdata, material, ShadingType, scale)
{
	m_orbitPoint = orbitPoint;
	m_radius = radius;
	m_velocity = angularVelocity;

	//a vector pointing from the orbit center to 
	m_direction = RotateY(theta) * vec3(0,0,-1);

	vec4 position = m_radius*m_direction + m_orbitPoint;
	Sphere::setLocation(position.x, position.y, position.z);
}

vec3 Planet::getOrbitPoint() const {

	return m_orbitPoint;
}

void Planet::orbit(GLfloat degrees) {

	m_direction = normalize(RotateY(degrees) * m_direction);
	vec4 position = m_radius*(m_direction) + m_orbitPoint;
	Sphere::setLocation(position.x, position.y, position.z);
}

void Planet::setOrbitPoint(const vec3& p) {

	m_orbitPoint = p;
}