#include "Input.h"

KeyboardState::KeyboardState()
{
	m_keys = new bool[SIZE];
	for (int i = 0; i < SIZE; i++)
		m_keys[i] = false;
}

KeyboardState::~KeyboardState()
{
	delete[] m_keys;
}

KeyboardState::KeyboardState(const KeyboardState& other)
{
	m_keys = new bool[SIZE];
	for (int i = 0; i < SIZE; i++)
		m_keys[i] = other.m_keys[i];
}

KeyboardState& KeyboardState::operator=(const KeyboardState& other)
{
	for (int i = 0; i < SIZE; i++)
		m_keys[i] = other.m_keys[i];
	return *this;
}

void KeyboardState::setKey(GLuint key, bool down)
{
	m_keys[key] = down;
}

void KeyboardState::setSpecialKey(GLuint key, bool down)
{
	m_keys[256 + key] = down;
}

bool KeyboardState::isDown(GLuint key) const
{
	return m_keys[key];
}