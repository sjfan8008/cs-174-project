#include "PlayerAction.h"
#include "Graphics.h"
#include "RigidBody.h"
#include "Input.h"
#include "Camera.h"

PlayerAction::PlayerAction() {

	m_stickVelocity = 0;
	m_shotInProgress = false;
}

PlayerAction::~PlayerAction() {

	//TODO: free any resources and heap memory
}

PlayerAction::PlayerAction(const PlayerAction& other) {

	//No need to implement this.  PlayerAction will not be copied
}

PlayerAction& PlayerAction::operator=(const PlayerAction& other) {

	//No need to implement this.  PlayerAction will not be assigned.
	return *this;
}

void PlayerAction::takeShotInit(const Ball& ball, Stick& stick, Camera3D& cam) {
	//TODO: initialized the position of the stick and camera



		// camera will face the direction of the ball's position
	vec3 vectorToBall = ball.getPosition()-cam.getPosition();
	cam.faceDirection(vectorToBall);
		// adjust pitch
	cam.setPitch(camInitPitch);
	
	vec3 deltaPosition = cam.getDirection() * (camInitDistance + ball.getRadius());
	cam.setCenter(ball.getPosition()-deltaPosition);

	vec3 camToBall = cam.getPosition()-ball.getPosition();
	m_camToBallDistance = sqrt((camToBall.x)*(camToBall.x) + (camToBall.y)*(camToBall.y) + (camToBall.z)*(camToBall.z)) - ball.getRadius();
	

			// the stick will face the same direction as the camera
	stick.faceDirection(cam.getDirection());
			// set desired pitch for stick angle
	stick.setPitch(stickPitch);
			// Translate the stick to the Center of the ball
	stick.setPosition(ball.getPosition().x,ball.getPosition().y,ball.getPosition().z);
			// Move the stick backward
	stick.moveParallelToCurrentDirection(-(stickMinDistance+ball.getRadius()));

	vec3 stickToBall = stick.getPosition() - ball.getPosition();
	m_stickToBallDistance = sqrt((stickToBall.x)*(stickToBall.x) + (stickToBall.y)*(stickToBall.y) + (stickToBall.z)*(stickToBall.z)) - ball.getRadius();

	//Suggestion: use stick.faceDirection() and cam.faceDirection() to set the direction
	//the stick and camera are facing.  Use stick.getDirection() cam.getDirection() to
	//get the direction they are currently facing.
}

int PlayerAction::takeShot(GLfloat timeDiff, const KeyboardState& keystate, const Ball& ball, Stick& stick, Camera3D& cam) {


	if (m_shotInProgress) {
		return animateStick(stick, ball, timeDiff);
	}

	GLfloat stickDelta = timeDiff * stickSpeed;
	GLfloat camAngleDelta = timeDiff * camAngleSpeed;

	
	vec3 stickToBall = stick.getPosition() - ball.getPosition();
	GLfloat stickToBallDistance = sqrt((stickToBall.x)*(stickToBall.x) + (stickToBall.y)*(stickToBall.y) + (stickToBall.z)*(stickToBall.z)) - ball.getRadius();
	//Note: read Camera.h for a description of some camera movement functions.  There is nothing
	//explicitly designed to rotate the camera around a point, but you can change the azimuth
	//of the camera with camera.rotateYaw(), and change the pitch of the camera with camera.rotatePitch().
	//You can query the camera's position with camera.getPosition(), and can set the position with camera.setCenter().

	if (keystate.isDown(KEY_LEFT)) {
		// Rotates the azimuth of 'cam' and 'stick' by -camAngleDelta degrees around the position of the ball 

		cam.rotateYaw(-camAngleDelta);
		vec3 deltaPosition = cam.getDirection() * (m_camToBallDistance + ball.getRadius());
		cam.setCenter(ball.getPosition()-deltaPosition);

		stick.rotate(vec3(0.0,1.0,0.0),-camAngleDelta);
		vec3 stickDeltaPosition = stick.getDirection() * (m_stickToBallDistance + ball.getRadius());
		stick.setPosition(ball.getPosition().x-stickDeltaPosition.x,ball.getPosition().y-stickDeltaPosition.y,ball.getPosition().z-stickDeltaPosition.z);
	}
	else if (keystate.isDown(KEY_RIGHT)) {
		// Rotates the azimuth of 'cam' and 'stick' by camAngleDelta degrees around the position of ball

		cam.rotateYaw(camAngleDelta);
		vec3 deltaPosition = cam.getDirection() * (m_camToBallDistance + ball.getRadius());
		cam.setCenter(ball.getPosition()-deltaPosition);

		stick.rotate(vec3(0.0,1.0,0.0),camAngleDelta);
		vec3 stickDeltaPosition = stick.getDirection() * (m_stickToBallDistance + ball.getRadius());
		stick.setPosition(ball.getPosition().x-stickDeltaPosition.x,ball.getPosition().y-stickDeltaPosition.y,ball.getPosition().z-stickDeltaPosition.z);
		
	}
	else if (keystate.isDown(KEY_UP)) {
		// Rotates the pitch of 'cam' by -camAngleDelta degrees around the position of 'ball'

		if(abs(DegreesToRadians*cam.getPitch()) < (89*DegreesToRadians))
		{
			cam.rotatePitch(-camAngleDelta);
			vec3 deltaPosition = cam.getDirection() * (m_camToBallDistance + ball.getRadius());
			cam.setCenter(ball.getPosition()-deltaPosition);
		}
		
	}
	else if (keystate.isDown(KEY_DOWN)) {
		// Rotates the pitch of 'cam' by camAngleDelta degrees around the position of 'ball'
		if(abs(DegreesToRadians*cam.getPitch()) >= (15*DegreesToRadians))	// limit 15 degree minimum angle
		{
			cam.rotatePitch(camAngleDelta);
			vec3 deltaPosition = cam.getDirection() * (m_camToBallDistance + ball.getRadius());
			cam.setCenter(ball.getPosition()-deltaPosition);
		}
	}
	else if (keystate.isDown('i')) {
		// Zoom camera in
		if(m_camToBallDistance >= camMinDistance)
		{
			m_camToBallDistance -= 0.5;
			vec3 deltaPosition = cam.getDirection() * (m_camToBallDistance + ball.getRadius());
			cam.setCenter(ball.getPosition()-deltaPosition);
		}
	}
	else if (keystate.isDown('o')) {
		// Zoom camera out
		if(m_camToBallDistance <= camMaxDistance)
		{
			m_camToBallDistance += 0.5;
			vec3 deltaPosition = cam.getDirection() * (m_camToBallDistance + ball.getRadius());
			cam.setCenter(ball.getPosition()-deltaPosition);
		}
	}
	else if (keystate.isDown('s')) {

		//TODO: Pull the stick backwards.  More specifically, do the following:
		//define a vector that is aligned with 'stick', and points in the direction
		//of the stick's tip.  Move the stick in the opposite direction of this vector
		//by an amount of stickDelta, unless doing so would place the tip of the stick
		//at a distance from the surface of 'ball' greater than stickMaxDistance.

		//Suggestion: use stick.moveParallelToCurrentDirection(-stickDelta)
		
		
			//if stick is at max distance already then don't go further back
		if(m_stickToBallDistance >= stickMaxDistance)
		{}
			//if stick is close to max distance don't go back a full delta
		else if(m_stickToBallDistance + stickDelta >= stickMaxDistance)
			stick.moveParallelToCurrentDirection(-(stickMaxDistance-stickToBallDistance));
		else
			stick.moveParallelToCurrentDirection(-stickDelta);

		// update stick position
		vec3 stickToBall = stick.getPosition() - ball.getPosition();
		m_stickToBallDistance = sqrt((stickToBall.x)*(stickToBall.x) + (stickToBall.y)*(stickToBall.y) + (stickToBall.z)*(stickToBall.z)) - ball.getRadius();

	}
	else if (keystate.isDown('w')) {

		//TODO: Move the stick forward.  As with moving the stick backward,
		//define a vector pointing along the stick, but this time move the
		//stick in the same direction of the vector by stickDelta units, unless
		//doing so would place the tip of the stick at a distance from the ball's
		//surface smaller than stickMinDistance.

		//Suggestion: use stick.moveParallelToCurrentDirection(stickDelta)

			//if stick is at min distance already then don't go closer
		if(m_stickToBallDistance <= stickMinDistance)
		{}
			//if stick is close to min distance don't go forward a full delta
		else if(m_stickToBallDistance - stickDelta <= stickMinDistance)
			stick.moveParallelToCurrentDirection((stickToBallDistance-stickMinDistance));
		else
			stick.moveParallelToCurrentDirection(stickDelta);

		// update stick position
		vec3 stickToBall = stick.getPosition() - ball.getPosition();
		m_stickToBallDistance = sqrt((stickToBall.x)*(stickToBall.x) + (stickToBall.y)*(stickToBall.y) + (stickToBall.z)*(stickToBall.z)) - ball.getRadius();
	}
	else if (keystate.isDown('e')) {

		//The player takes their shot.  Now every time takeShot is called
		//the stick must be moved towards the ball until it hits the ball,
		//at which point it should return TOOK_SHOT
		m_stickVelocity = m_stickToBallDistance / shotAnimationTime;
		m_shotInProgress = true;
	}
	else if (keystate.isDown('q')) {

		//TODO: any clean-up or state changes that are necessary
		return SHOT_CANCELLED;
	}

	
	stickToBall = stick.getPosition() - ball.getPosition();
	stickToBallDistance = sqrt((stickToBall.x)*(stickToBall.x) + (stickToBall.y)*(stickToBall.y) + (stickToBall.z)*(stickToBall.z)) - ball.getRadius();

	
	if(abs(stickToBallDistance-stickMinDistance) <= TOLERANCE)
		m_force = normalize(stick.getDirection())*stickMinForce;
	else if(abs(stickToBallDistance-stickMaxDistance) <= TOLERANCE)
		m_force = normalize(stick.getDirection())*stickMaxForce;
	else
	{
		GLfloat proportionOfForce = (stickToBallDistance-stickMinDistance)/(stickMaxDistance-stickMinDistance);
		m_force = normalize(stick.getDirection())*proportionOfForce*stickMaxForce;
	}

	//TODO: Here we should update the current force of the player's shot.
	//The force will scale proportional to the amount the stick has been pulled
	//back.  When the stick's tip is at stickMinDistance, the force will have a 
	//magnitude of stickMinForce.  When the stick is at stickMaxDistance the
	//force will have a magnitude of stickMaxForce.  The force must point in
	//the same direction as the stick, eg. pointing at the ball.

	return ACTION_INCOMPLETE;
}

int PlayerAction::animateStick(Stick& stick, const Ball& ball, GLfloat timeDiff) {

	//TODO: move the stick forward, eg. along a vector aligned parallel
	//to the stick, until the tip of the stick is touching the ball's surface.
	//The amount of time taken for the tip of the stick to reach the ball's
	//surface must be constant, and is given by stickAnimationTime.  This means
	//the stick will move at faster speeds when it is pulled back farther.  


	//TODO: you will have to use the distance between the stick's initial position and the
	//ball's surface to calculate stickVelocity.
	
	

		//This will keep a constant shotAnimationTime if the velocity is proportional to it
	
	GLfloat stickDelta = timeDiff * m_stickVelocity;


	//Suggestion: use stick.moveParallelToCurrentDirection(stickDelta)

	//TODO: if the stick has reached the ball, set m_shotInProgress to false
	//and return TOOK_SHOT.  Else return ACTION_INCOMPLETE.

		//if stick is going to move too far, only move stick until it reaches the ball
	if(m_stickToBallDistance - stickDelta <= 0)
	{
		stick.moveParallelToCurrentDirection((m_stickToBallDistance));
		m_shotInProgress = false;
		return TOOK_SHOT;
	}
	
	else
	{
		stick.moveParallelToCurrentDirection(stickDelta);
		m_stickToBallDistance -= stickDelta;
	}

	return ACTION_INCOMPLETE;
}

const vec3* PlayerAction::getShotForce() const {

	//TODO: return the force of the player's shot.
	return &m_force;
}