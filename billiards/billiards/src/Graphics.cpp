#include "Graphics.h"
#include "IL\ilut.h"
#include <cassert>

/*****************************************************************/
//							Movable
/*****************************************************************/

Movable::Movable(GLfloat x, GLfloat y, GLfloat z, GLfloat scale, const Quaternion& rotation)
	: m_rotation(rotation)
{
	m_x = x;
	m_y = y;
	m_z = z;
	m_scale = scale;
}

Movable::~Movable() {

}

vec3 Movable::getPosition() const {
	return vec3(m_x, m_y, m_z);
}

void Movable::setPosition(GLfloat x, GLfloat y, GLfloat z) {
	m_x = x;
	m_y = y;
	m_z = z;
}

void Movable::rotate(const vec3& axis, GLfloat degrees) {

	m_rotation = Quaternion(axis, degrees) * m_rotation;
}


void Movable::setYaw(GLfloat degrees) {

	GLfloat currentPitch = 180/M_PI * asin( getDirection().y );
	m_rotation = Quaternion(vec3(0,1,0), degrees) * Quaternion(vec3(1,0,0), currentPitch);
}


void Movable::setPitch(GLfloat degrees) {
	
	vec3 dir = getDirection();
	GLfloat yaw = 180/M_PI * atan2(-dir.x, -dir.z);
	m_rotation = Quaternion(vec3(0,1,0), yaw) * Quaternion(vec3(1,0,0), degrees);
}

void Movable::faceDirection(const vec3& direction) {

	m_rotation = Quaternion(direction);
}

vec3 Movable::getDirection() const {

	return normalize(m_rotation * vec3(0,0,-1));
}

void Movable::translate(GLfloat dx, GLfloat dy, GLfloat dz) {
	m_x += dx;
	m_y += dy;
	m_z += dz;
}

void Movable::moveParallelToCurrentDirection(GLfloat s)
{
	vec3 forwardDir = m_rotation * vec3(0,0,-1);
	forwardDir = normalize(forwardDir);
	m_x += s*forwardDir.x;
	m_y += s*forwardDir.y;
	m_z += s*forwardDir.z;
}

void Movable::scale(GLfloat s) {

	if (s > 0) {
		m_scale = s;
	}
}

mat4 Movable::worldTransform() const {

	return Translate(m_x, m_y, m_z) * (m_rotation.getMatrix()) * Scale(m_scale, m_scale, m_scale);
}

/*****************************************************************/
//							Model
/*****************************************************************/

Model::Model() 
	: Movable(0,0,0,1.0, Quaternion(1.0,0,0,0))
{
	m_init = false;
}

Model::Model(const ModelData& mdata)
	: Movable(0,0,0,1.0,Quaternion(1.0,0,0,0))
{
	m_bufMan = mdata.bufMan;
	m_texture = mdata.texture;
	m_material = mdata.material;
	m_shadingType = mdata.shadingType;
	m_init = true;
}

void Model::Init(const ModelData& mdata)
{
	m_bufMan = mdata.bufMan;
	m_texture = mdata.texture;
	m_material = mdata.material;
	m_shadingType = mdata.shadingType;
	m_init = true;
}

Model::~Model() {

}

Model::Model(const Model& other)
	: Movable(other)
{
}

Model& Model::operator=(const Model& other) {

	return *this;
}

void Model::draw() const {

	int bi = this->getBufferDataIndex();
	if ( ! m_bufMan->isLoaded(bi) )
		return;

	m_bufMan->bindBuffer();
	GLuint vao = m_bufMan->getVertexAttributeObject( bi );
	GLuint nVertices = m_bufMan->getNumVertices( bi );
	glBindVertexArray(vao);

	glBindTexture( GL_TEXTURE_2D, m_texture );

	glDrawArrays(GL_TRIANGLES, 0, nVertices);
	glBindVertexArray(0);
}

Material Model::getMaterial() const
{
	return m_material;
}

GLuint Model::getShadingType() const
{
	return m_shadingType;
}

/*****************************************************************/
//							Shader
/*****************************************************************/

Shader::Shader(GLuint program) {

	//get locations of all attributes

	//vertex arrays
	m_vPosition = glGetAttribLocation(program, "vPosition");
	m_normal = glGetAttribLocation(program, "vNormal" );
	m_vTexCoord = glGetAttribLocation(program, "vTexCoord" );

	//transformations
	m_ModelView = glGetUniformLocation(program, "ModelView");
	m_Projection = glGetUniformLocation(program, "Projection");
	m_Camera = glGetUniformLocation(program, "Camera");

	//lighting attributes
	m_AmbientProduct = glGetUniformLocation(program, "AmbientProduct");
	m_SpecularProduct = glGetUniformLocation(program, "SpecularProduct");
	m_DiffuseProduct = glGetUniformLocation(program, "DiffuseProduct");
	m_Shininess = glGetUniformLocation(program, "Shininess");
	m_ShadingType = glGetUniformLocation(program, "ShadingType");
	m_lightPosition = glGetUniformLocation(program, "LightPosition");

	m_renderMode = glGetUniformLocation(program, "renderMode");
	m_pickColor = glGetUniformLocation(program, "pickColor");
}

void Shader::setModelView(const mat4& mat) {
	glUniformMatrix4fv(m_ModelView, 1, GL_TRUE, mat);
}

void Shader::setProjection(const mat4& mat) {
	glUniformMatrix4fv(m_Projection, 1, GL_TRUE, mat);
}

void Shader::setCam(const mat4& mat) {
	glUniformMatrix4fv(m_Camera, 1, GL_TRUE, mat);
}

void Shader::setLighting(const LightSource* light, const Material& material) {

	glUniform4fv(m_lightPosition, 1, point4(light->getPosition(), 1.0));
	glUniform4fv(m_AmbientProduct, 1, light->ambientProductOn(material.ambient));
	glUniform4fv(m_DiffuseProduct, 1, light->diffuseProductOn(material.diffuse));
	glUniform4fv(m_SpecularProduct, 1, light->specularProductOn(material.specular));
    glUniform1f(m_Shininess, material.shininess);
}

void Shader::setShadingType(GLuint shadingType) {
	glUniform1i(m_ShadingType, shadingType);
}

void Shader::setColor(const GLubyte color[3]) {
	color4 vcolor(color[0]/255.0, color[1]/255.0, color[2]/255.0, 1.0);
	glUniform4fv(m_pickColor, 1, vcolor);
}

void Shader::setRenderMode(int mode) {
	glUniform1i(m_renderMode, mode);
}

/*****************************************************************/
//						Buffer Data
/*****************************************************************/

BufferData::BufferData() {
	points = NULL; normals = NULL; textCoords = NULL; nVertices = 0; 
}

/*****************************************************************/
//						BufferManager
/*****************************************************************/

BufferManager::BufferManager(const Shader* pShader) {
	m_shader = pShader;
	glGenBuffers( 1, &m_buffer );
	m_nData = 0;
	m_bufferSize = 0;
}

bool BufferManager::isLoaded(GLuint index) const {
	return (0 <= index && index < m_nData);
}

int BufferManager::loadBufferData(const BufferData& g) {

	m_data.push_back(g);
	int ret = m_nData;
	m_nData++;
	m_bufferSize += g.getDataSize();
	return ret;
}

void BufferManager::createGLBuffers() {

	glBindBuffer( GL_ARRAY_BUFFER, m_buffer );

	glBufferData( GL_ARRAY_BUFFER, m_bufferSize, NULL, GL_STATIC_DRAW);

	//copy points to the buffer object
	GLuint offset = 0;
	for (GLuint i = 0; i < m_nData; i++) {

		GLuint nVertices = m_data[i].nVertices;
		point4* points = m_data[i].points;
		vec3* normals = m_data[i].normals;
		vec2* textCoords = m_data[i].textCoords;

		//copy points to the buffer object
		GLuint poffset = offset;
		glBufferSubData( GL_ARRAY_BUFFER, offset, nVertices*sizeof(point4), points );
		offset += nVertices*sizeof(point4);

		//copy normals to the buffer object
		GLuint noffset = offset;
		glBufferSubData( GL_ARRAY_BUFFER, offset, nVertices*sizeof(vec3), normals );
		offset += nVertices*sizeof(vec3);

		//copy texture coordinates to the buffer object
		GLuint toffset = offset;
		glBufferSubData( GL_ARRAY_BUFFER, offset, nVertices*sizeof(vec2), textCoords );
		offset += nVertices*sizeof(vec2);

		//initialize vertex arrays
		GLuint vao;
		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);
		
		glEnableVertexAttribArray( m_shader->getPosLoc() );
		glVertexAttribPointer( m_shader->getPosLoc(), 4, GL_FLOAT, GL_FALSE, 0,
				BUFFER_OFFSET(poffset) );
		glEnableVertexAttribArray( m_shader->getNormalLoc() );
		glVertexAttribPointer( m_shader->getNormalLoc(), 3, GL_FLOAT, GL_FALSE, 0,
				BUFFER_OFFSET(noffset) );
		glEnableVertexAttribArray( m_shader->getTextureLoc() );
		glVertexAttribPointer( m_shader->getTextureLoc(), 2, GL_FLOAT, GL_FALSE, 0,
				BUFFER_OFFSET(toffset) );

		m_data[i].vao = vao;
		glBindVertexArray(0);
	}
}

void BufferManager::bindBuffer() const {
	glBindBuffer( GL_ARRAY_BUFFER, m_buffer );
}

void BufferManager::freeCPUmemory() {

}

void BufferManager::unload() {

}

GLuint BufferManager::getVertexAttributeObject(GLuint index) const {
	assert(0 <= index && index < m_nData);
	return m_data[index].vao;
}

GLuint BufferManager::getNumVertices(GLuint index) const {
	assert(0 <= index && index < m_nData);
	return m_data[index].nVertices;
}

/*****************************************************************/
//						Texture
/*****************************************************************/

Texture::Texture(const char filename[]) {

	assert(filename);

	//load image
	ILuint ImageName;
	ilGenImages(1, &ImageName);
	ilBindImage(ImageName);

	ilLoadImage(filename);

	m_width = ilGetInteger(IL_IMAGE_WIDTH);
	m_height = ilGetInteger(IL_IMAGE_HEIGHT);
	GLubyte* image = new GLubyte[m_width*m_height*3];
	ilCopyPixels(0, 0, 0, m_width, m_height, 1, IL_RGB, IL_UNSIGNED_BYTE, image);

	ilBindImage(0);
	ilDeleteImage(ImageName);

	GLuint texture[1];
	glGenTextures(1, texture );
    glBindTexture( GL_TEXTURE_2D, *texture);
    glTexImage2D( GL_TEXTURE_2D, 0, GL_RGB, m_width, m_height, 0,
		  GL_RGB, GL_UNSIGNED_BYTE, image );

	//done copying image to GPU
	delete[] image;

    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );

	m_texture = *texture;
}

GLuint Texture::width() const {
	return m_width;
}

GLuint Texture::height() const {
	return m_height;
}

GLuint Texture::getGLtextObject() const {
	return m_texture;
}

