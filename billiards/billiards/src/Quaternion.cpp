#include "Quaternion.h"

Quaternion::Quaternion(GLfloat W, GLfloat X, GLfloat Y, GLfloat Z)
	: w(W), x(X), y(Y), z(Z)
{
}

Quaternion::Quaternion(const vec3 &axis, GLfloat angle, bool APPROX)
{
	//global scope operator required so we don't use Quaternion::normalize
	vec4 vNormal(::normalize(axis));

	//convert to radians
	angle = DegreesToRadians * angle;

	GLfloat halfAngle = angle / 2.0;
	
	//q = cos(angle/2) + sin(angle/2)*vNormal
	GLfloat sinHalfAngle = APPROX ? halfAngle : sin(halfAngle);
	w = APPROX ? 1.0 : cos(halfAngle);
	x = sinHalfAngle * vNormal.x;
	y = sinHalfAngle * vNormal.y;
	z = sinHalfAngle * vNormal.z;
}

Quaternion::Quaternion(const vec3& direction) {

	GLfloat angle = acos( dot( ::normalize(direction), vec3(0,0,-1) ) );
	vec3 axis = cross(vec3(0,0,-1), direction);
	vec3 normal = ::normalize(axis);
	
	GLfloat halfAngle = angle / 2.0;
	
	//q = cos(angle/2) + sin(angle/2)*vNormal
	GLfloat sinHalfAngle = sin(halfAngle);
	w = cos(halfAngle);
	x = sinHalfAngle * normal.x;
	y = sinHalfAngle * normal.y;
	z = sinHalfAngle * normal.z;
}

vec3 Quaternion::operator*(const vec3 &v)
{
	//v = qvq' where q is this quaternion, q' is the complex conjugate of q,
	//and v is the resulting vector.

	vec4 vn(::normalize(v));
 
	Quaternion vecQuat(0, vn.x, vn.y, vn.z);
	Quaternion resQuat = (*this) * (vecQuat * this->getConjugate());
 
	return vec3(resQuat.x, resQuat.y, resQuat.z);
}


Quaternion Quaternion::operator*(const Quaternion &q2)
{
	//w = w1*w2 - v1 DOT v2
	//v = w1v2 + w2v1 + v1 CROSS v2
	return Quaternion(w * q2.w - x * q2.x - y * q2.y - z * q2.z,
					  w * q2.x + x * q2.w + y * q2.z - z * q2.y,
	                  w * q2.y + y * q2.w + z * q2.x - x * q2.z,
	                  w * q2.z + z * q2.w + x * q2.y - y * q2.x
	                  );
}

void Quaternion::normalize()
{
	// Don't normalize if we don't have to
	GLfloat mag2 = w * w + x * x + y * y + z * z;
	if (fabs(mag2) > Q_NORMAL_TOLERANCE && fabs(mag2 - 1.0f) > Q_NORMAL_TOLERANCE) {
		GLfloat mag = sqrt(mag2);
		w /= mag;
		x /= mag;
		y /= mag;
		z /= mag;
	}
}

Quaternion Quaternion::getConjugate()
{
	this->normalize();
	return Quaternion(w, -x, -y, -z);
}

void Quaternion::negate()
{
	w *= -1;
	x *= -1;
	y *= -1;
	z *= -1;
}

mat4 Quaternion::getMatrix()
{
	GLfloat x2 = x * x;
	GLfloat y2 = y * y;
	GLfloat z2 = z * z;
	GLfloat xy = x * y;
	GLfloat xz = x * z;
	GLfloat yz = y * z;
	GLfloat wx = w * x;
	GLfloat wy = w * y;
	GLfloat wz = w * z;
 
	//The following computation requires this quaternion to be normalized.
	this->normalize();

	return mat4( 1.0f - 2.0f * (y2 + z2), 2.0f * (xy - wz), 2.0f * (xz + wy), 0.0f,
				 2.0f * (xy + wz), 1.0f - 2.0f * (x2 + z2), 2.0f * (yz - wx), 0.0f,
				 2.0f * (xz - wy), 2.0f * (yz + wx), 1.0f - 2.0f * (x2 + y2), 0.0f,
				 0.0f,			   0.0f,			 0.0f,					  1.0f);
}