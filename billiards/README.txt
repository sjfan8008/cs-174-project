Team Members:

Michael Gendler, Yoshi Osone, Corey Quon, Ian Yarbrough

********************************	Overview		********************************

This application is designed to simulate a 'practice' game of billiards.  There are 
no strict rules--a player can shoot any ball, any way they want.  The emphasis here
is on the physics of ball motion.

********************************   Advanced Topics	********************************

1.	Collision Detection

	This topic is integral to the application logic.  Each time a ball changes position
	the game engine must check if it has collided with either a wall, a pocket, or
	another ball, and take appropriate action.  Collisions between balls are modeled
	as elastic collisions, where as collisions with a ball and the wall cause the ball 
	to lose a small amount of energy.  Collisions with a pocket cause the ball to
	disappear.

2.	Picking

	Each of the 16 balls is pickable, so long as it is not in a pocket.  We used colorbuffer
	picking because it was easy to implement using glReadPixels().
	

********************************	 Gameplay		********************************

The game consists of three phases.  In the first phase the player surveys the 
table and selects a ball to take a shot at by clicking on the ball with the 
mouse cursor.  The player can select any ball--not just the cue ball--as the
point is to have fun shooting balls, and also demonstrate picking.  

During this phase the player is free to move the camera anywhere using the following keys:

a				move laterally left
d				move laterally right
w				move in the direction the camera is facing
s				move in the opposite direction that the camera is facing
left arrow		rotate the camera left
right arrow		rotate the camera right
up arrow		rotate the camera up
down arrow		rotate the camera down
r				reset the camera to its initial position
q				exit the application

The second phase takes place after the player has selected a ball with the mouse cursor.  
During this phase the stick and camera lock on to the ball.  The player can adjust their
shot, shoot, or deselect the ball.  The controls during this phase are as follows:

s				increase shot force
w				decrease shot force
e				shoot
left arrow		rotate shot angle left
right arrow		rotate shot angle right
up arrow		rotate camera up
down arrow		rotate camera down
i				zoom camera in
o				zoom camera out
q				deselect ball (return to game phase 1)

The third phase happens after the player shoots the ball, and is completely out of the player's
control.  During this phase the balls move and collide until either all balls are in a pocket,
or no balls are still moving.  


********************************  Building from the source ********************************

This project depends on the GLut, Glew and DevIL frameworks.  Headers, dlls, and static libraries
for all three are included in the project under include, dll, and lib folders, respectively.
If visual studio cannot find these files, it may be because the respective path variables in the
property sheet are not relative to the project directory, but an absolute path.  If this is the case,
open the property sheet and change the include and lib paths to the previously mentioned 'include'
and 'lib' folders.

-- Character Set

	Before building the project, make sure visual studio is configured to use Multi-byte character set.
	


********************************	System Specs		********************************


This application was tested in the following environment:

OS: Windows
OpenGL version: 3.3
CPU:	Intel Core 2 Duo
System Type:	x64/x86
Graphics Card: nvidia GeForce 9800M GTS

We consider these to be the mimimum system specs.  It is a known issue that this application
performs poorly on older graphics cards, and possibly with older versions of openGL.
